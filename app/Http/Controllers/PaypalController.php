<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Invoice;
use App\InvoicePayment;
use App\Order;
use App\Plan;
use App\Product;
use App\ProductVariantOption;
use App\Shipping;
use App\Store;
use App\UserCoupon;
use App\UserDetail;
use App\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PaypalController extends Controller
{
    private $_api_context;

    public function setApiContext($slug = '')
    {
        if(Auth::check())
        {
            $paypal_conf['settings']['mode'] = env('PAYPAL_MODE');
            $paypal_conf                     = config('paypal');
        }
        else
        {
            $store                           = Store::where('slug', $slug)->first();
            $paypal_conf['settings']['mode'] = $store['paypal_mode'];
            $paypal_conf['client_id']        = $store['paypal_client_id'];
            $paypal_conf['secret_key']       = $store['paypal_secret_key'];
        }
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret_key']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function PayWithPaypal(Request $request, $slug)
    {

        $cart     = session()->get($slug);
        $products = $cart['products'];

        $store   = Store::where('slug', $slug)->first();
        $objUser = \Auth::user();

        $total        = 0;
        $sub_tax      = 0;
        $sub_total    = 0;
        $total_tax    = 0;
        $product_name = [];
        $product_id   = [];
        foreach($products as $team_products){
            foreach($team_products as $key => $product)
            {
                if($product['variant_id'] != 0)
                {

                    $product_name[] = $product['product_name'];
                    $product_id[]   = $key;

                    foreach($product['tax'] as $tax)
                    {
                        $sub_tax   = ($product['variant_price'] * $product['quantity'] * $tax['tax']) / 100;
                        $total_tax += $sub_tax;
                    }
                    $totalprice = $product['variant_price'] * $product['quantity'];
                    $total      += $totalprice;
                }
                else
                {
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $key;

                    foreach($product['tax'] as $tax)
                    {
                        $sub_tax   = ($product['price'] * $product['quantity'] * $tax['tax']) / 100;
                        $total_tax += $sub_tax;
                    }
                    $totalprice = $product['price'] * $product['quantity'];
                    $total      += $totalprice;
                }
            }
        }
        if($products)
        {
            try
            {
                $coupon_id = null;
                $price     = $total + $total_tax;

                if(isset($cart['shipping']) && isset($cart['shipping']['shipping_id']) && !empty($cart['shipping']))
                {
                    $shipping = Shipping::find($cart['shipping']['shipping_id']);
                    if(!empty($shipping))
                    {
                        $price = $price + $shipping->price;
                    }
                }

                $this->setApiContext($slug);
                $name  = implode(',', $product_name);
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');
                $item_1 = new Item();
                $item_1->setName($name)->setCurrency($store->currency_code)->setQuantity(1)->setPrice($price);
                $item_list = new ItemList();
                $item_list->setItems([$item_1]);
                $amount = new Amount();
                $amount->setCurrency($store->currency_code)->setTotal($price);
                $transaction = new Transaction();
                $transaction->setAmount($amount)->setItemList($item_list)->setDescription($name);
                $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(
                    route('get.payment.status', $store->slug)
                )->setCancelUrl(
                    route('get.payment.status', $store->slug)
                );
                $payment = new Payment();
                $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions([$transaction]);
                try
                {
                    $payment->create($this->_api_context);
                }
                catch(\PayPal\Exception\PayPalConnectionException $ex) //PPConnectionException
                {
                    return redirect()->back()->with('error', $ex->getMessage());
                }
                foreach($payment->getLinks() as $link)
                {
                    if($link->getRel() == 'approval_url')
                    {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }
                Session::put('paypal_payment_id', $payment->getId());
                if(isset($redirect_url))
                {
                    return Redirect::away($redirect_url);
                }

                return redirect()->back()->with('error', __('Unknown error occurred'));
            }
            catch(\Exception $e)
            {
                return redirect()->back()->with('error', __('Unknown error occurred'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('is deleted.'));
        }
    }

    public function GetPaymentStatus(Request $request, $slug)
    {

        $cart         = session()->get($slug);
        $products     = $cart['products'];
        $store        = Store::where('slug', $slug)->first();
        $user_details = $cart['customer'];

        $total        = 0;
        $new_qty      = 0;
        $sub_total    = 0;
        $total_tax    = 0;
        $product_name = [];
        $product_id   = [];
        $quantity     = [];
        $pro_tax      = [];
        foreach($products as $team_products){

            foreach($team_products as $key => $product)
            {
                if($product['variant_id'] != 0)
                {
                    //                dd($product);
                    $new_qty                = $product['originalvariantquantity'] - $product['quantity'];
                    $product_edit           = ProductVariantOption::find($product['variant_id']);
                    $product_edit->quantity = $new_qty;
                    $product_edit->save();

                    $product_name[] = $product['product_name'];
                    $product_id[]   = $key;
                    $quantity[]     = $product['quantity'];


                    foreach($product['tax'] as $tax)
                    {
                        $sub_tax   = ($product['variant_price'] * $product['quantity'] * $tax['tax']) / 100;
                        $total_tax += $sub_tax;
                        $pro_tax[] = $sub_tax;
                    }
                    $totalprice = $product['variant_price'] * $product['quantity'] + $total_tax;
                    $subtotal   = $product['variant_price'] * $product['quantity'];
                    $sub_total  += $subtotal;
                    $total      += $totalprice;
                }
                else
                {
                    $new_qty                = $product['originalquantity'] - $product['quantity'];
                    $product_edit           = Product::find($product['product_id']);
                    $product_edit->quantity = $new_qty;
                    $product_edit->save();

                    $product_name[] = $product['product_name'];
                    $product_id[]   = $key;
                    $quantity[]     = $product['quantity'];


                    foreach($product['tax'] as $tax)
                    {
                        $sub_tax   = ($product['price'] * $product['quantity'] * $tax['tax']) / 100;
                        $total_tax += $sub_tax;
                        $pro_tax[] = $sub_tax;
                    }
                    $totalprice = $product['price'] * $product['quantity'] + $total_tax;
                    $subtotal   = $product['price'] * $product['quantity'];
                    $sub_total  += $subtotal;
                    $total      += $totalprice;
                }
            }
        }
        if(isset($cart['shipping']) && isset($cart['shipping']['shipping_id']) && !empty($cart['shipping']))
        {
            $shipping = Shipping::find($cart['shipping']['shipping_id']);
            if(!empty($shipping))
            {
                $shipping_name  = $shipping->name;
                $shipping_price = $shipping->price;

                $shipping_data = json_encode(
                    [
                        'shipping_name' => $shipping_name,
                        'shipping_price' => $shipping_price,
                        'location_id' => $cart['shipping']['location_id'],
                    ]
                );
            }
            else
            {
                $shipping_data = '';
            }
        }
        $user = Auth::user();

        if($product)
        {
            $this->setApiContext($slug);
            $payment_id = Session::get('paypal_payment_id');
            Session::forget('paypal_payment_id');
            if(empty($request->PayerID || empty($request->token)))
            {
                return redirect()->route('store-payment.payment', $slug)->with('error', __('Payment failed'));
            }
            $payment   = Payment::get($payment_id, $this->_api_context);
            $execution = new PaymentExecution();
            $execution->setPayerId($request->PayerID);
            try
            {
                $result = $payment->execute($execution, $this->_api_context)->toArray();

                $order          = new Order();
                $order->user_id = Auth()->id();
                $latestOrder    = Order::orderBy('created_at', 'DESC')->first();
                if(!empty($latestOrder))
                {
                    $order->order_nr = '#' . str_pad($latestOrder->id + 1, 4, "100", STR_PAD_LEFT);
                }
                else
                {
                    $order->order_nr = '#' . str_pad(1, 4, "100", STR_PAD_LEFT);

                }
                $orderID = $order->order_nr;
                $status  = ucwords(str_replace('_', ' ', $result['state']));
                if($result['state'] == 'approved')
                {
                    $order                  = new Order();
                    $order->order_id        = $orderID;
                    $order->name            = $user_details['name'];
                    $order->card_number     = '';
                    $order->card_exp_month  = '';
                    $order->card_exp_year   = '';
                    $order->status          = 'pending';
                    $order->user_address_id = $user_details['id'];
                    $order->shipping_data   = $shipping_data;
                    $order->price           = $result['transactions'][0]['amount']['total'];
                    $order->product         = json_encode($products);
                    $order->price_currency  = $store->currency_code;
                    $order->txn_id          = $payment_id;
                    $order->payment_type    = __('PAYPAL');
                    $order->payment_status  = $result['state'];
                    $order->receipt         = '';
                    $order->user_id         = $store['id'];
                    $order->save();

                    session()->forget($slug);

                    return redirect()->route(
                        'store-complete.complete', [
                                                     $store->slug,
                                                     Crypt::encrypt($order->id),
                                                 ]
                    )->with('success', __('Transaction has been ' . __($status)));
                }
                else
                {

                    return redirect()->back()->with('error', __('Transaction has been ' . __($status)));
                }
            }
            catch(\Exception $e)
            {
                return redirect()->back()->with('error', __('Transaction has been failed.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __(' is deleted.'));
        }
    }

}
