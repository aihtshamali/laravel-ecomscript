@extends('layouts.admin')
@section('page-title')
    {{__('Store')}}
@endsection
@section('title')
    <div class="d-inline-block">
        <h5 class="h4 d-inline-block text-white font-weight-bold mb-0 ">{{__('Store')}}</h5>
    </div>
@endsection
@section('breadcrumb')
@endsection
@section('action-btn')
    <a href="{{ route('store.grid') }}" data-title="{{__('Create New Store')}}" class="btn btn-sm btn-white bor-radius">
        {{__('Grid View')}}
    </a>
    <a href="#" data-size="lg" data-url="{{ route('store-resource.create') }}" data-ajax-popup="true" data-title="{{__('Create New Store')}}" class="btn btn-sm btn-white btn-icon-only rounded-circle">
        <i class="fa fa-plus"></i>
    </a>
@endsection
@section('filter')
@endsection
@push('css-page')
    <link rel="stylesheet" href="{{asset('assets/libs/summernote/summernote-bs4.css')}}">

@endpush
@push('script-page')
    <script src="{{asset('assets/libs/summernote/summernote-bs4.js')}}"></script>
@endpush
@section('content')
    <!-- Listing -->
    <div class="card">
        <!-- Card header -->
        <div class="card-header actions-toolbar">
            <div class="actions-search" id="actions-search">
                <div class="input-group input-group-merge input-group-flush">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-transparent"><i class="far fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control form-control-flush" placeholder="Type and hit enter ...">
                    <div class="input-group-append">
                        <a href="#" class="input-group-text bg-transparent" data-action="search-close" data-target="#actions-search"><i class="far fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between align-items-center">
                <div class="col">
                    <h6 class="d-inline-block mb-0">{{__('All Store')}}</h6>
                </div>
            </div>
        </div>
        <!-- Table -->
        <div class="table-responsive">
            <table class="table align-items-center">
                <thead>
                <tr>
                    <th scope="col">{{ __('Store Name')}}</th>
                    <th scope="col">{{ __('Email')}}</th>
                    <th scope="col">{{ __('Link')}}</th>
                    <th scope="col">{{ __('Plan')}}</th>
                    <th scope="col">{{ __('Created At')}}</th>
                    <th scope="col" class="text-right">{{ __('Action')}}</th>

                </tr>
                </thead>
                <tbody class="list">
                @foreach($stores as $store)
                    @foreach($users as $user)
                        <tr>
                            <td>
                                {{$store->name}}
                            </td>
                            <td>
                                {{$store->email}}
                            </td>
                            <td>
                                <a href="{{route('store.slug',$store->slug)}}" target="_blank">{{route('store.slug',$store->slug)}}</a>
                            </td>
                            <td>
                                {{!empty($store->store_user->user->currentPlan->name)?$store->store_user->user->currentPlan->name:'-'}}
                            </td>
                            <td>
                                {{\App\Utility::dateFormat($store->created_at)}}
                            </td>

                            <td class="text-right">
                                <!-- Actions -->
                                <div class="actions ml-3">
                                    <a href="#" data-size="lg" data-url="{{ route('store-resource.edit',$store->id) }}" data-ajax-popup="true" class="action-item" data-title="{{__('Edit Store')}}" data-toggle="tooltip" title="" data-original-title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a href="#" class="action-item" data-size="lg" data-url="{{ route('plan.upgrade',$user->id) }}" data-ajax-popup="true" data-toggle="tooltip" data-title="{{__('Upgrade Plan')}}">
                                        <i class="fas fa-trophy"></i>
                                    </a>
                                    <a href="#" class="action-item" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').' | '.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$store->id}}').submit();">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['store-resource.destroy', $store->id],'id'=>'delete-form-'.$store->id]) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script-page')
    <script>
        $(document).on('click', '#billing_data', function () {
            $("[name='shipping_address']").val($("[name='billing_address']").val());
            $("[name='shipping_city']").val($("[name='billing_city']").val());
            $("[name='shipping_state']").val($("[name='billing_state']").val());
            $("[name='shipping_country']").val($("[name='billing_country']").val());
            $("[name='shipping_postalcode']").val($("[name='billing_postalcode']").val());
        })
    </script>
@endpush

