<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="StoreGo - Business Ecommerce">
    <meta name="author" content="Rajodiya Infotech">

    <title>{{'Product Details'}} - {{($store->tagline) ?  $store->tagline : config('APP_NAME', 'StoreGo')}}</title>

    <link rel="icon" href="{{asset(Storage::url('uploads/logo/').(!empty($settings->value)?$settings->value:'favicon.png'))}}" type="image/png">

    <link rel="stylesheet" href="{{asset('assets/libs/@fortawesome/fontawesome-free/css/all.min.css')}}"><!-- Page CSS -->
    <link rel="stylesheet" href="{{asset('assets/libs/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libs/swiper/dist/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}" id="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libs/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}" id="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" id="stylesheet')}}">
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.1.min.js')}}"></script>
</head>
<style>
    .shoping_count:after {
        content: attr(value);
        font-size: 14px;
        background: #273444;
        border-radius: 50%;
        padding: 1px 5px 1px 4px;
        position: relative;
        left: -5px;
        top: -10px;
    }

    @media (min-width: 768px) {
        .header-account-page {
            height: 100px;
        }
    }
</style>
<body>
@php
    if(!empty(session()->get('lang')))
    {
        $currantLang = session()->get('lang');
    }else{
        $currantLang = $store->lang;
    }
    $languages=\App\Utility::languages();
@endphp
<header class="header" id="header-main">
    <!-- Topbar -->
    <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark border-bottom">
        <div class="container px-0">
            <div class="navbar-nav align-items-center">
                <a class="navbar-brand mr-lg-4" href="{{route('store.slug',$store->slug)}}">
                    @if(!empty($store->logo))
                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/store_logo/'.$store->logo))}}" id="navbar-logo" style="height: 40px;">
                    @else
                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/store_logo/logo.png'))}}" id="navbar-logo" style="height: 40px;">
                    @endif
                </a>
                <div class="d-none d-lg-inline-block">
                    <span class="navbar-text mr-3 mt-2 text-lg">{{ucfirst($store->name)}}</span>
                </div>
                <div class="ml-auto">
                    <ul class="nav">
                        <li class="nav-item dropdown ml-lg-2">
                            <div class="dropdown-menu dropdown-menu-sm">
                                @foreach($languages as $language)
                                    <a href="{{route('change.languagestore',[$store->slug,$language])}}" class="dropdown-item @if($language == $currantLang) active-language @endif">
                                        <span> {{Str::upper($language)}}</span>
                                    </a>
                                @endforeach
                            </div>
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="text-sm mb-0"><i class="fas fa-globe-asia"></i>
                                    {{Str::upper($currantLang)}}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('store.cart',$store->slug)}}">
                                <i class="fa badge shoping_count" id="shoping_count" style="font-size:16px" value="{{!empty($total_item)?$total_item:'0'}}">&#xf07a;</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-content">
    <header class="p-3 d-flex align-items-end">
        <!-- Header container -->
        <div class="container">
            <div class="row">
                <div class=" col-lg-12">
                    <!-- Salute + Small stats -->
                    <div class="row align-items-center ">
                        <div class="col-md-5 mb-4 mb-md-0">
                            <span class="h2 mb-0 text-dark d-block">{{__('Product')}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card {{ ($store_setting->enable_rating == 'off')?'card-fluid':''}}">
                        <div class="card-body">
                            <!-- Product title -->
                            <h5 class="h4">{{$products->name}}</h5>
                            <!-- Rating -->
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    @if($store_setting->enable_rating == 'on')
                                        @for($i =1;$i<=5;$i++)
                                            @php
                                                $icon = 'fa-star';
                                                $color = '';
                                                $newVal1 = ($i-0.5);
                                                if($avg_rating < $i && $avg_rating >= $newVal1)
                                                {
                                                    $icon = 'fa-star-half-alt';
                                                }
                                                if($avg_rating >= $newVal1)
                                                {
                                                    $color = 'text-warning';
                                                }
                                            @endphp
                                            <i class="fas {{$icon .' '. $color}}"></i>
                                        @endfor
                                        {{$avg_rating}}/5 ({{$user_count}} {{__('reviews')}})
                                    @endif
                                </div>
                                <div class="col-sm-6 text-sm-right">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <span class="badge badge-pill badge-soft-info">{{__('ID: #')}}{{$products->SKU}}</span>
                                        </li>
                                        <li class="list-inline-item">
                                            @if($products->quantity == 0)
                                                <span class="badge badge-pill badge-soft-danger">
                                            {{__('Out of stock')}}
                                        </span>
                                            @else
                                                <span class="badge badge-pill badge-soft-success">
                                            {{__('In stock')}}
                                        </span>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Description -->
                            {!! $products->description !!}
                        </div>
                    </div>
                    @if($store_setting->enable_rating == 'on')
                        <div class="card">
                            <div class="card-body p-3">
                                <h5 class="float-left mb-0 pt-2">{{__('Rating')}}</h5>
                                <a href="#" class="btn btn-sm btn-primary btn-icon-only rounded-circle float-right text-white" data-size="lg" data-toggle="modal-rating" data-target="commonModal" data-url="{{route('rating',[$store->slug,$products->id])}}" data-ajax-popup="true" data-title="{{__('Create New Rating')}}">
                                    <i class="fas fa-plus"></i>
                                </a>
                            </div>
                            @foreach($product_ratings as $product_key => $product_rating)
                                @if($product_rating->rating_view == 'on')
                                    <div id="review_list" class="px-3 pt-2 border-top pb-0">
                                        <div class="theme-review float-left" id="comment_126267">
                                            <div class="theme_review_item">
                                                <div class="theme-review__heading">
                                                    <div class="theme-review__heading__item text-sm small">
                                                        <h6>{{$product_rating->title}}</h6>
                                                        <tr class="list-dotted ">
                                                            <td class="list-dotted__item">by {{$product_rating->name}} :</td>
                                                            <td class="list-dotted__item">{{$product_rating->created_at->diffForHumans()}}</td>
                                                        </tr>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rate float-right">
                                            @for($i =0;$i<5;$i++)
                                                <i class="fas fa-star {{($product_rating->ratting > $i  ? 'text-warning' : '')}}"></i>
                                            @endfor
                                        </div>
                                        <span class="clearfix"></span>
                                        <br>
                                        <div class="main_reply_body">
                                            <p class="small">{{$product_rating->description}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="col-lg-6">
                    @if($products->enable_product_variant =='on')
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <input type="hidden" id="product_id" value="{{$products->id}}">
                                    <input type="hidden" id="variant_id" value="">
                                    <input type="hidden" id="variant_qty" value="">
                                    @foreach($product_variant_names as $key => $variant)
                                        <div class="col-sm-6 mb-4 mb-sm-0">
                                            <span class="d-block h6 mb-0">
                                                <th><span>{{ $variant->variant_name }}</span></th>

                                                <select name="product[{{$key}}]" id="pro_variants_name" class="form-control custom-select variant-selection  pro_variants_name{{$key}}">
                                                    <option value="">{{ __('Select')  }}</option>
                                                @foreach($variant->variant_options as $key => $values)
                                                        <option value="{{$values}}">{{$values}}</option>
                                                    @endforeach
                                            </select>
                                        </span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-sm-6 mb-4 mb-sm-0">
                                    <span class="d-block h3 mb-0 variasion_price">
                                        @if($products->enable_product_variant =='on')
                                            {{\App\Utility::priceFormat(0)}}
                                        @else
                                            {{\App\Utility::priceFormat($products->price)}}
                                        @endif
                                    </span>
                                </div>
                                <div class="col-sm-6 text-sm-right">
                                    <a href="#" data-id="{{$products->id}}" class="add_to_cart btn btn-sm btn-danger btn-icon-only rounded-circle float-right text-white" data-size="lg" data-id="{{$products->id}}"  data-title="Add team members">
                                        <i class="fas fa-plus"></i>
                                    </a>  
                                    <!-- <button type="button" class="btn btn-primary btn-icon">
                                        <span class="btn-inner--icon">
                                                <i class="fas fa-shopping-cart"></i>
                                        </span>
                                        <span class="btn-inner--text">{{__('Add to cart')}}</span>
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Product images -->
                    <div class="card">
                        <div class="card-body">
                            <a href="{{asset(Storage::url('uploads/is_cover_image/'.$products->is_cover))}}" data-fancybox data-caption="My caption">
                                @if(!empty($products->is_cover))
                                    <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/'.$products->is_cover))}}" class="img-fluid rounded">
                                @else
                                    <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/default.jpg'))}}" class="img-fluid rounded">
                                @endif
                            </a>
                            <div class="row mt-4">
                                @foreach($products_image as $key => $productss)
                                    <div class="col-4">
                                        <div class="p-3 border rounded">
                                            <a href="{{asset(Storage::url('uploads/product_image/'.$products_image[$key]->product_images))}}" class="stretched-link" data-fancybox="product">
                                                @if(!empty($products_image[$key]->product_images))
                                                    <a href="{{asset(Storage::url('uploads/product_image/'.$products_image[$key]->product_images))}}" class="stretched-link" data-fancybox="product">
                                                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/product_image/'.$products_image[$key]->product_images))}}" class="img-fluid">
                                                    </a>
                                                @else
                                                    <a href="{{asset(Storage::url('uploads/product_image/de'))}}" class="stretched-link" data-fancybox="product">
                                                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/product_image/default.jpg'))}}" class="img-fluid">
                                                    </a>
                                                @endif
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4 text-center m-3">
            <a href="{{route('store.slug',$store->slug)}}" class="btn btn-link text-sm text-white badge-dark bor-radius py-2">{{__('Return to shop')}}</a>
        </div>
    </section>
     <!-- Modal -->
    <div class="modal fade team_modal" id="dialog_confirm_map_{{$products->id}}" style="overflow:scroll;" tabindex="-1" role="dialog" aria-labelledby="dialog_confirm_mapLabel" aria-hidden="true">													
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-6">
                        <h6> Choose Team Member(s) For This</h6>
                    </div>
                    <div class="col-md-6">
                        <h6> Product Image</h6>
                    </div>                                                                    
                </div>
                <div class="modal-body">
                    <div class="row" style="max-height:280px">
                        <div class="col-sm-4 col-md-4 team_members" id="modal-body_{{$products->id}}" style="border-right: 2px solid #eff2f7;">
                            <!-- HERE -->
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="modal_product_image">
                                @if(!empty($products->is_cover))
                                    <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/'.$products->is_cover))}}" class="img-center pro_max_width60">
                                @else
                                    <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/default.jpg'))}}" class="img-center pro_max_width60">
                                @endif
                            </div>
                            <div class="product_description">
                                <p>{{$products->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"> 
                    <div class="col-md-6">
                        <label style="font-size:12px">Add Team Members</label> <br/>
                        <input type="text" style="display:inline;width: 70%;height:auto;padding: 0.3rem;" name="new_member" class="form-control" id="">
                        <button class="btn btn-xs btn-success ml-2 add_member"><i class="fas fa-plus"></i></button>
                    </div>
                    <div class="col-md-6" style="align-self: flex-end;">
                        <div class="btn-group pull-right">
                            <button data-modal="_{{$products->id}}" data-id="{{$products->id}}" data-target="dialog_confirm_map_{{$products->id}}" data-main="dialog_confirm_map_{{$products->id}}" class="submit-button btn-xs btn btn-success">Submit</button>
                            <button data-dismiss="modal" class="btn-xs cancel-btn btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<footer id="footer-main">
    <div class="footer footer-dark pt-4 pb-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright text-sm font-weight-bold text-center text-md-left pt-2">
                        {{$store->footer_note}}
                    </div>
                </div>
                <div class="col-md-6">
                    <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                        @if(!empty($store->email))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->email}}" target="_blank">
                                    <i class="fas fa-envelope"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->whatsapp))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->whatsapp}}" target=”_blank”>
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->facebook))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->facebook}}" target="_blank">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->instagram))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->instagram}}" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->twitter))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->twitter}}" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->youtube))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->youtube}}" target="_blank">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="commonModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header align-items-center">
                    <div class="modal-title">
                        <h6 class="mb-0" id="modelCommanModelLabel"></h6>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Core JS - includes jquery, bootstrap, popper, in-view and sticky-kit -->
<script src="{{asset('assets/js/site.core.js')}}"></script>
<!-- notify -->
<script type="text/javascript" src="{{ asset('assets/js/custom.js')}}"></script>
<script src="{{ asset('assets/libs/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{ asset('assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js')}}"></script>
<!-- Page JS -->
<script src="{{asset('assets/libs/swiper/dist/js/swiper.min.js')}}"></script>
<!-- Site JS -->
<script src="{{asset('assets/js/site.js')}}"></script>
<!-- Demo JS - remove it when starting your project -->
<script src="{{asset('assets/js/demo.js')}}"></script>
@php
    $store_settings = \App\Store::where('slug',$store->slug)->first();
@endphp
{!! $store_settings->storejs !!}
<script async src="https://www.googletagmanager.com/gtag/js?id={{$store_settings->google_analytic}}"></script>

<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '{{ $store_settings->google_analytic }}');
</script>
<script>
    // $(".add_to_cart").click(function (e) {
    //     e.preventDefault();
    //     var id = $(this).attr('data-id');
    //     var variants = [];
    //     $(".variant-selection").each(function (index, element) {
    //         variants.push(element.value);
    //     });

    //     if (jQuery.inArray('', variants) != -1) {
    //         show_toastr('Error', "{{ __('Please select all option.') }}", 'error');
    //         return false;
    //     }
    //     var variasion_ids = $('#variant_id').val();

    //     $.ajax({
    //         type: "POST",
    //         url: '{{route('user.addToCart', ['__product_id',$store->slug,'variasion_id'])}}'.replace('__product_id', id).replace('variasion_id', variasion_ids),
    //         data: {
    //             "_token": "{{ csrf_token() }}",
    //         },
    //         success: function (response) {
    //             if (response.status == "Success") {
    //                 show_toastr('Success', response.success, 'success');
    //                 $("#shoping_count").attr("value", response.item_count);
    //             } else {
    //                 show_toastr('Error', response.error, 'error');
    //             }
    //         },
    //         error: function (result) {
    //             console.log('error');
    //         }
    //     });
    // });

    $(document).on('change', '#pro_variants_name', function () {

        var variants = [];
        $(".variant-selection").each(function (index, element) {
            variants.push(element.value);
        });
        if (variants.length > 0) {
            $.ajax({
                url: '{{route('get.products.variant.quantity')}}',
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    variants: variants.join(' : '),
                    product_id: $('#product_id').val()
                },

                success: function (data) {
                    console.log(data);
                    $('.variasion_price').html(data.price);
                    $('#variant_id').val(data.variant_id);
                    $('#variant_qty').val(data.quantity);
                }
            });
        }
    });
</script>
@if(Session::has('success'))
    <script>
        show_toastr('{{__('Success')}}', '{!! session('success') !!}', 'success');
    </script>
    {{ Session::forget('success') }}
@endif
@if(Session::has('error'))
    <script>
        show_toastr('{{__('Error')}}', '{!! session('error') !!}', 'error');
    </script>
    {{ Session::forget('error') }}
@endif

<script>
    let member_names = [];
    function fetchMembers(product_id = null){
         $.ajax({
            type: "GET",
            url: '{{route('user.getMembers', [$store->slug])}}',
            data: {
                "_token": "{{ csrf_token() }}",
                "product_id": product_id
            },
            success: function (response) {
                console.log(response);
                if (response.status == "Success" && response.team_members && response.team_members) {
                    let team_elem = $(".team_members");
                    team_elem.children().remove();
                    for (const [key, value] of Object.entries(response.team_members)) {
                        team_elem.append(`<div>
                            <input type="checkbox" name="team_member[]" value="${key}" id="" ${value}> <span class="member_name"> ${key.toUpperCase()} </span>
                        </div>`);    
                    }
                }
                // if (response.status == "Success" && response.team_members && response.team_members.length) {
                //     member_names = response.team_members
                //     response.team_members.forEach(member => {
                //         console.log(member);
                //     });
                // }

            },
            error: function (result) {
                show_toastr('Error', 'Something went wrong!', 'error');
                console.log('error',result);
            }
        });
    }
    $("button.add_member").on("click",function(){
        const memberElem = $(this).siblings("input[name='new_member']")[0]
        const member_name = memberElem.value
        if(member_name && member_name.trim().length && !member_names.includes(member_name.trim().toLowerCase())){
            let member = member_name.trim().toLowerCase()
             $.ajax({
                type: "POST",
                url: '{{route('user.storeMember', [$store->slug])}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "team_member": member
                },
                success: function (response) {
                    if (response.status == "Success") {
                         $(".team_members").append(`<div>
                            <input type="checkbox" name="team_member[]" value="${response.team_member}" id=""> <span class="member_name"> ${response.team_member.toUpperCase()} </span>
                        </div>`);
                        memberElem.value = ""
                        member_names.push(member);
                        show_toastr('Success', response.success, 'success');
                    }

                },
                error: function (result) {
                    show_toastr('Error', 'Something went wrong!', 'error');
                    console.log('error',result);
                }
            });
           
        }
    })
     //getting click event to show modal
    $('.add_to_cart').click(function (e) {
        e.preventDefault();
		$(`#commonModal`).modal("hide");  
		let id = $(this).data("id")
        fetchMembers(id);
		$(`#dialog_confirm_map_${id}`).modal();  
      //appending modal background inside the bigform-content
		$('.modal-backdrop').appendTo('.bigform-content');
      //removing body classes to able click events
		$('body').removeClass();
    });
    $(".submit-button").click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var modalId = $(this).data('main');
        var selected_member_names = $("#modal-body"+$(this).data("modal")).find("input:checked").map(function(){
            return $(this).val();
        }).get();
        if(!selected_member_names || selected_member_names.length < 1){
            show_toastr('Error', "You need to select atleast one member", 'error');
            return;
        }
        var variants = [];
        $(".variant-selection").each(function (index, element) {
            variants.push(element.value);
        });

        if (jQuery.inArray('', variants) != -1) {
            show_toastr('Error', "{{ __('Please select all variant option.') }}", 'error');
            return false;
        }
        var variasion_ids = $('#variant_id').val();

        $.ajax({
            type: "POST",
            url: '{{route('user.addToCart', ['__product_id',$store->slug,'variasion_id'])}}'.replace('__product_id', id).replace('variasion_id', variasion_ids),
            data: {
                "_token": "{{ csrf_token() }}",
                "team_members": selected_member_names
            },
            success: function (response) {
                if (response.status == "Success") {
                    show_toastr('Success', response.success, 'success');
                    $("#shoping_count").attr("value", response.item_count);
                    $(`#${modalId}`).modal('toggle'); // closing the modal
                } else {
                    show_toastr('Error', response.error, 'error');
                }
            },
            error: function (result) {
                console.log('error');
            }
        });
    });
</script>
</body>
</html>

