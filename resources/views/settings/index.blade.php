@extends('layouts.admin')
@php
    $store_logo=asset(Storage::url('uploads/store_logo/'));
    $logo=asset(Storage::url('uploads/logo/'));
    $lang=\App\Utility::getValByName('default_language');
    if(Auth::user()->type == 'Owner')
    {
        $store_lang=$store_settings->lang;
    }
@endphp
@section('page-title')
    {{__('Store Setting')}}
@endsection
@section('title')
    <div class="d-inline-block">
        <h5 class="h4 d-inline-block font-weight-bold mb-0 text-white">{{__('Store Setting')}}</h5>
    </div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{__('Home')}}</a></li>
@endsection
@section('action-btn')
@endsection
@section('filter')
@endsection
@push('css-page')
    <style>
        hr {
            margin: 8px;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/libs/summernote/summernote-bs4.css')}}">
@endpush
@push('script-page')
    <script src="{{asset('assets/libs/summernote/summernote-bs4.js')}}"></script>
@endpush
@section('content')
    <div class="mt-4">
        <div class="card">
            <ul class="nav nav-tabs nav-overflow profile-tab-list" role="tablist">
                <li class="nav-item ml-4">
                    <a href="#store_setting" id="store_setting_tab" class="nav-link active" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                        <i class="fas fa-store mr-2"></i>
                        {{__('Store Settings')}}
                    </a>
                </li>
                <li class="nav-item ml-4">
                    <a href="#site_setting" id="site_setting_tab" class="nav-link" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                        <i class="fas fa-cog mr-2"></i>{{__('Site Setting')}}
                    </a>
                </li>
                <li class="nav-item ml-4">
                    <a href="#payment-setting" id="payment-setting_tab" class="nav-link" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                        <i class="fab fa-cc-visa mr-2"></i>{{__('Store Payment')}}
                    </a>
                </li>
                <li class="nav-item ml-4">
                    <a href="#email_setting" id="system_setting_tab" class="nav-link" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                        <i class="fas fa-envelope mr-2"></i>{{__('Email Setting')}}
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="store_setting" role="tabpanel" aria-labelledby="orders-tab">
                    {{Form::model($store_settings,array('route'=>array('settings.store',$store_settings['id']),'method'=>'POST','enctype' => "multipart/form-data"))}}
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="logo" class="form-control-label">{{ __('Logo') }}</label>
                                    <input type="file" name="logo" id="logo" class="custom-input-file">
                                    <label for="logo">
                                        <i class="fa fa-upload"></i>
                                        <span>{{__('Choose a file')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-6 d-flex align-items-center justify-content-center mt-3">
                                <div class="logo-div">
                                    <img src="{{$store_logo.'/'.(isset($store_settings['logo']) && !empty($store_settings['logo'])?$store_settings['logo']:'logo.png')}}" width="170px" class="img_setting">
                                </div>
                            </div>
                            <div class="col-12">
                                @error('logo')
                                <div class="row">
                                    <span class="invalid-logo" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('store_name',__('Store Name'),array('class'=>'form-control-label')) }}
                                {!! Form::text('name',null,array('class'=>'form-control','placeholder'=>__('Store Name'))) !!}
                                @error('store_name')
                                <span class="invalid-store_name" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('email',__('Email'),array('class'=>'form-control-label')) }}
                                {{Form::text('email',null,array('class'=>'form-control','placeholder'=>__('Email')))}}
                                @error('email')
                                <span class="invalid-email" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="col-6 py-4">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_domain" id="enable_domain" {{ ($store_settings['enable_domain'] == 'on') ? 'checked=checked' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_domain">{{__('Custom Domain')}}</label>
                                </div>
                                <span class="sundomain text-sm">{{__('Note : Before add custom domain, your domain A record is pointing to our server IP : '.$serverIp.'')}}
                                  <br>  {{ __('Type : A, Name : @ or subdomain, value : '.$serverIp.'')}}
                                </span>
                            </div>
                            <div class="form-group col-md-6" id="StoreLink">
                                {{Form::label('store_link',__('Store Link'),array('class'=>'form-control-label')) }}
                                <div class="input-group">
                                    <input type="text" value="{{ $store_settings['store_url'] }}" id="myInput" class="form-control d-inline-block" aria-label="Recipient's username" aria-describedby="button-addon2" readonly>
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="button" onclick="myFunction()" id="button-addon2"><i class="far fa-copy"></i> {{__('Copy Link')}}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6 sundomain">
                                {{Form::label('store_domain',__('Custom Domain'),array('class'=>'form-control-label')) }}
                                {{Form::text('domains',$store_settings['domains'],array('class'=>'form-control','placeholder'=>__('Enter Domain')))}}
                            </div>
                            <div class="form-group col-md-12 sundomain">
                                <span class="sundomain">{{__('Note : If you\'re using cPanel or Plesk then you need to manually add your custom domain in your server with the same root directory as the script\'s installation. and user need to point their custom domain A record with your server IP '.$serverIp.')')}}</span>
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('tagline',__('Tagline'),array('class'=>'form-control-label')) }}
                                {{Form::text('tagline',null,array('class'=>'form-control','placeholder'=>__('Tagline')))}}
                                @error('tagline')
                                <span class="invalid-tagline" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('address',__('Address'),array('class'=>'form-control-label')) }}
                                {{Form::text('address',null,array('class'=>'form-control','placeholder'=>__('Address')))}}
                                @error('address')
                                <span class="invalid-address" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('city',__('City'),array('class'=>'form-control-label')) }}
                                {{Form::text('city',null,array('class'=>'form-control','placeholder'=>__('City')))}}
                                @error('city')
                                <span class="invalid-city" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('state',__('State'),array('class'=>'form-control-label')) }}
                                {{Form::text('state',null,array('class'=>'form-control','placeholder'=>__('State')))}}
                                @error('state')
                                <span class="invalid-state" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('zipcode',__('Zipcode'),array('class'=>'form-control-label')) }}
                                {{Form::text('zipcode',null,array('class'=>'form-control','placeholder'=>__('Zipcode')))}}
                                @error('zipcode')
                                <span class="invalid-zipcode" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('country',__('Country'),array('class'=>'form-control-label')) }}
                                {{Form::text('country',null,array('class'=>'form-control','placeholder'=>__('Country')))}}
                                @error('country')
                                <span class="invalid-country" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('store_default_language',__('Store Default Language')) }}
                                <div class="changeLanguage">
                                    <select name="store_default_language" id="store_default_language" class="form-control custom-select" data-toggle="select">
                                        @foreach(\App\Utility::languages() as $language)
                                            <option @if($store_lang == $language) selected @endif value="{{$language }}">{{Str::upper($language)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('country',__('Product Rating Display'),array('class'=>'form-control-label mb-3')) }}
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_rating" id="enable_rating" {{ ($store_settings['enable_rating'] == 'on') ? 'checked=checked' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_rating"></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <i class="fab fa-google" aria-hidden="true"></i>
                                    {{Form::label('google_analytic',__('Google Analytic'),array('class'=>'form-control-label')) }}
                                    {{Form::text('google_analytic',null,array('class'=>'form-control','placeholder'=>__('UA-XXXXXXXXX-X')))}}
                                    @error('google_analytic')
                                    <span class="invalid-google_analytic" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('Shipping Method Enable',__('Shipping Method Enable'),array('class'=>'form-control-label mb-3')) }}
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_shipping" id="enable_shipping" {{ ($store_settings['enable_shipping'] == 'on') ? 'checked=checked' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_shipping"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                {{Form::label('about',__('About'),array('class'=>'form-control-label')) }}
                                {{Form::textarea('about',null,array('class'=>'form-control summernote-simple','rows'=>3,'placehold   er'=>__('About')))}}
                                @error('about')
                                <span class="invalid-about" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="col-12 pt-4">
                                <h5 class="h6 mb-0">{{__('Footer Note')}}</h5>
                                <small>{{__('This detail will use for make explore social media.')}}</small>
                                <hr class="my-4">
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fas fa-envelope"></i>
                                    {{Form::label('email',__('Email'),array('class'=>'form-control-label')) }}
                                    {{Form::text('email',null,array('class'=>'form-control','rows'=>3,'placeholder'=>__('Email')))}}
                                    @error('email')
                                    <span class="invalid-email" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                                    {{Form::label('whatsapp',__('Whatsapp'),array('class'=>'form-control-label')) }}
                                    {{Form::text('whatsapp',null,array('class'=>'form-control','rows'=>3,'placeholder'=>__('https://wa.me/1XXXXXXXXXX')))}}
                                    @error('whatsapp')
                                    <span class="invalid-whatsapp" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fab fa-facebook-square" aria-hidden="true"></i>
                                    {{Form::label('facebook',__('Facebook'),array('class'=>'form-control-label')) }}
                                    {{Form::text('facebook',null,array('class'=>'form-control','rows'=>3,'placeholder'=>__('https://www.facebook.com/')))}}
                                    @error('facebook')
                                    <span class="invalid-facebook" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fab fa-instagram" aria-hidden="true"></i>
                                    {{Form::label('instagram',__('Instagram'),array('class'=>'form-control-label')) }}
                                    {{Form::text('instagram',null,array('class'=>'form-control','placeholder'=>__('https://www.instagram.com/')))}}
                                    @error('instagram')
                                    <span class="invalid-instagram" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                    {{Form::label('twitter',__('Twitter'),array('class'=>'form-control-label')) }}
                                    {{Form::text('twitter',null,array('class'=>'form-control','placeholder'=>__('https://twitter.com/')))}}
                                    @error('twitter')
                                    <span class="invalid-twitter" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fab fa-youtube" aria-hidden="true"></i>
                                    {{Form::label('youtube',__('Youtube'),array('class'=>'form-control-label')) }}
                                    {{Form::text('youtube',null,array('class'=>'form-control','placeholder'=>__('https://www.youtube.com/')))}}
                                    @error('youtube')
                                    <span class="invalid-youtube" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="fas    fa-copyright" aria-hidden="true"></i>
                                    {{Form::label('footer_note',__('Footer Note'),array('class'=>'form-control-label')) }}
                                    {{Form::text('footer_note',null,array('class'=>'form-control','placeholder'=>__('Footer Note')))}}
                                    @error('footer_note')
                                    <span class="invalid-footer_note" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                {{Form::label('storejs',__('Store Custom JS'),array('class'=>'form-control-label')) }}
                                {{Form::textarea('storejs',null,array('class'=>'form-control','rows'=>3,'placehold   er'=>__('Store Custom JS')))}}
                                @error('storejs')
                                <span class="invalid-storejs" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>

                            <div class="col-12 pt-4">
                                <h5 class="h6 mb-0">{{__('Header Setting')}}</h5>
                                <small>{{__('This detail will use to change header setting.')}}</small>
                                <hr class="my-4">
                            </div>
                            <div class="col-12 py-4">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_header_img" id="enable_header_img" {{ ($store_settings['enable_header_img'] == 'on') ? 'checked=checked' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_header_img">{{__('Display Header')}}</label>
                                </div>
                            </div>
                            <div id="headerimg" class="col-md-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="logo" class="form-control-label">{{ __('Header Image') }}</label>
                                            <input type="file" name="header_img" id="header_img" class="custom-input-file">
                                            <label for="header_img">
                                                <i class="fa fa-upload"></i>
                                                <span>{{__('Choose a file')}}</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('header_title',__('Header Title'),array('class'=>'form-control-label')) }}
                                            {{Form::text('header_title',null,array('class'=>'form-control','placeholder'=>__('Enter Header Title')))}}
                                            @error('header_title')
                                            <span class="invalid-header_title" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('header_desc',__('Header Description'),array('class'=>'form-control-label')) }}
                                            {{Form::text('header_desc',null,array('class'=>'form-control','placeholder'=>__('Enter Header Description')))}}
                                            @error('header_desc')
                                            <span class="invalid-header_desc" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('button_text',__('Header Button Text'),array('class'=>'form-control-label')) }}
                                            {{Form::text('button_text',null,array('class'=>'form-control','placeholder'=>__('Enter Header Button Text')))}}
                                            @error('button_text')
                                            <span class="invalid-button_text" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-4">
                                <h5 class="h6 mb-0">{{__('Email Subscriber Setting')}}</h5>
                                <small>{{__('This detail will use to change header setting.')}}</small>
                                <hr class="my-4">
                            </div>
                            <div class="col-12 py-4">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_subscriber" id="enable_subscriber" {{ ($store_settings['enable_subscriber'] == 'on') ? 'checked=checked' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_subscriber">{{__('Display Email Subscriber Box')}}</label>
                                </div>
                            </div>
                            <div id="subscriber" class="col-md-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="logo" class="form-control-label">{{ __('Subscriber Background Image') }}</label>
                                            <input type="file" name="sub_img" id="sub_img" class="custom-input-file">
                                            <label for="sub_img">
                                                <i class="fa fa-upload"></i>
                                                <span>{{__('Choose a file')}}</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('subscriber_title',__('Subscriber Title'),array('class'=>'form-control-label')) }}
                                            {{Form::text('subscriber_title',null,array('class'=>'form-control','placeholder'=>__('Enter Subscriber Title')))}}
                                            @error('subscriber_title')
                                            <span class="invalid-subscriber_title" role="alert">
                                                <strong class="text-danger">{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{Form::label('sub_title',__('Subscriber Sub Title'),array('class'=>'form-control-label')) }}
                                            {{Form::text('sub_title',null,array('class'=>'form-control','placeholder'=>__('Enter Subscriber Sub Title')))}}
                                            @error('sub_title')
                                            <span class="invalid-sub_title" role="alert">
                                         <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <div class="row">
                            <div class="col-6 text-left">
                                <button type="button" class="btn btn-sm btn-soft-danger btn-icon rounded-pill" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').' | '.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$store_settings->id}}').submit();">
                                    <span class="btn-inner--text">{{__('Delete Store')}}</span>
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                {{Form::submit(__('Save Change'),array('class'=>'btn btn-sm btn-primary rounded-pill'))}}
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                {!! Form::open(['method' => 'DELETE', 'route' => ['ownerstore.destroy', $store_settings->id],'id'=>'delete-form-'.$store_settings->id]) !!}
                {!! Form::close() !!}
                <div class="tab-pane fade show" id="site_setting" role="tabpanel" aria-labelledby="orders-tab">
                    {{Form::model($settings,array('route'=>'business.setting','method'=>'POST','enctype' => "multipart/form-data"))}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="full_logo" class="form-control-label">{{ __('Logo') }}</label>
                                    <input type="file" name="logo" id="full_logo" class="custom-input-file">
                                    <label for="full_logo">
                                        <i class="fa fa-upload"></i>
                                        <span>{{__('Choose a file')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-6 d-flex align-items-center justify-content-center mt-3">
                                <div class="logo-div">
                                    @if(!empty($store_settings->logo))
                                        <img src="{{asset(Storage::url('uploads/store_logo/'.$store_settings->logo))}}" width="170px" class="img_setting">
                                    @else
                                        <img src="{{asset(Storage::url('uploads/logo/logo.png'))}}" width="170px" class="img_setting">
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="favicon" class="form-control-label">{{ __('Favicon') }}</label>
                                    <input type="file" name="favicon" id="favicon" class="custom-input-file">
                                    <label for="favicon">
                                        <i class="fa fa-upload"></i>
                                        <span>{{__('Choose a file')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-6 d-flex align-items-center justify-content-center mt-3">
                                <div class="logo-div">
                                    <img src="{{$logo.'/'.(isset($company_favicon) && !empty($company_favicon)?$company_favicon:'favicon.png')}}" width="50px" class="img_setting">
                                </div>
                            </div>
                            <div class="col-12">
                                @error('logo')
                                <div class="row">
                                    <span class="invalid-logo" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{Form::label('title_text',__('Title Text')) }}
                                {{Form::text('title_text',null,array('class'=>'form-control','placeholder'=>__('Title Text')))}}
                                @error('title_text')
                                <span class="invalid-title_text" role="alert">
                                     <strong class="text-danger">{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_text',__('Footer Text')) }}
                                {{Form::text('footer_text',null,array('class'=>'form-control','placeholder'=>__('Footer Text')))}}
                                @error('footer_text')
                                <span class="invalid-footer_text" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('default_language',__('Default Language')) }}
                                <div class="changeLanguage">
                                    <select name="default_language" id="default_language" class="form-control custom-select" data-toggle="select">
                                        @foreach(\App\Utility::languages() as $language)
                                            <option @if($lang == $language) selected @endif value="{{$language }}">{{Str::upper($language)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('display_landing_page_',__('Landing Page Display')) }}
                                <div class="col-12 mt-2">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="display_landing_page" id="display_landing_page" {{ $settings['display_landing_page'] == 'on' ? 'checked="checked"' : '' }}>
                                        <label class="custom-control-label form-control-label" for="display_landing_page"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="site_date_format" class="form-control-label">{{__('Date Format')}}</label>
                                <select type="text" name="site_date_format" class="form-control selectric" id="site_date_format">
                                    <option value="M j, Y" @if(@$settings['site_date_format'] == 'M j, Y') selected="selected" @endif>Jan 1,2015</option>
                                    <option value="d-m-Y" @if(@$settings['site_date_format'] == 'd-m-Y') selected="selected" @endif>d-m-y</option>
                                    <option value="m-d-Y" @if(@$settings['site_date_format'] == 'm-d-Y') selected="selected" @endif>m-d-y</option>
                                    <option value="Y-m-d" @if(@$settings['site_date_format'] == 'Y-m-d') selected="selected" @endif>y-m-d</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="site_time_format" class="form-control-label">{{__('Time Format')}}</label>
                                <select type="text" name="site_time_format" class="form-control selectric" id="site_time_format">
                                    <option value="g:i A" @if(@$settings['site_time_format'] == 'g:i A') selected="selected" @endif>10:30 PM</option>
                                    <option value="g:i a" @if(@$settings['site_time_format'] == 'g:i a') selected="selected" @endif>10:30 pm</option>
                                    <option value="H:i" @if(@$settings['site_time_format'] == 'H:i') selected="selected" @endif>22:30</option>
                                </select>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{Form::label('footer_link_1',__('Footer Link Title 1')) }}
                                {{Form::text('footer_link_1',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link Title 1')))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_value_1',__('Footer Link href 1')) }}
                                {{Form::text('footer_value_1',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link 1')))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_link_2',__('Footer Link Title 2')) }}
                                {{Form::text('footer_link_2',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link Title 2')))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_value_2',__('Footer Link href 2')) }}
                                {{Form::text('footer_value_2',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link 2')))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_link_3',__('Footer Link Title 3')) }}
                                {{Form::text('footer_link_3',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link Title 3')))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('footer_value_3',__('Footer Link href 3')) }}
                                {{Form::text('footer_value_3',null,array('class'=>'form-control','placeholder'=>__('Enter Footer Link 3')))}}
                            </div>

                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {{Form::submit(__('Save Change'),array('class'=>'btn btn-sm btn-primary rounded-pill'))}}
                    </div>
                    {{Form::close()}}
                </div>
                <div class="tab-pane fade show" id="payment-setting" role="tabpanel" aria-labelledby="orders-tab">
                    <div class="card-body">
                        {{Form::open(array('route'=>array('owner.payment.setting',$store_settings->slug),'method'=>'post'))}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{Form::label('currency_symbol',__('Currency Symbol *')) }}
                                    {{Form::text('currency_symbol',$store_settings['currency'],array('class'=>'form-control','required'))}}
                                    @error('currency_symbol')
                                    <span class="invalid-currency_symbol" role="alert">
                                            <strong class="text-danger">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{Form::label('currency',__('Currency *')) }}
                                    {{Form::text('currency',$store_settings['currency_code'],array('class'=>'form-control font-style','required'))}}
                                    {{__('Note: Add currency code as per three-letter ISO code.')}}
                                    <small>
                                        <a href="https://stripe.com/docs/currencies" target="_blank">{{__('you can find out here..')}}</a>
                                    </small>
                                    @error('currency')
                                    <span class="invalid-currency" role="alert">
                                            <strong class="text-danger">{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols3Input">{{__('Currency Symbol Position')}}</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-control custom-radio mb-3">
                                                <input type="radio" id="customRadio5" name="currency_symbol_position" value="pre" class="custom-control-input" @if(@$store_settings['currency_symbol_position'] == 'pre') checked @endif>
                                                <label class="custom-control-label" for="customRadio5">{{__('Pre')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-control custom-radio mb-3">
                                                <input type="radio" id="customRadio6" name="currency_symbol_position" value="post" class="custom-control-input" @if(@$store_settings['currency_symbol_position'] == 'post') checked @endif>
                                                <label class="custom-control-label" for="customRadio6">{{__('Post')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols3Input">{{__('Currency Symbol Space')}}</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-control custom-radio mb-3">
                                                <input type="radio" id="customRadio7" name="currency_symbol_space" value="with" class="custom-control-input" @if(@$store_settings['currency_symbol_space'] == 'with') checked @endif>
                                                <label class="custom-control-label" for="customRadio7">{{__('With Space')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-control custom-radio mb-3">
                                                <input type="radio" id="customRadio8" name="currency_symbol_space" value="without" class="custom-control-input" @if(@$store_settings['currency_symbol_space'] == 'without') checked @endif>
                                                <label class="custom-control-label" for="customRadio8">{{__('Without Space')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-6 py-2">
                                <h5 class="h5 mb-0">{{__('Cash On Delivery')}}</h5>
                                <small> {{__('Note : Enable or disable cash on delivery.')}}</small><br>
                                <small> {{__('This detail will use for make checkout of shopping cart.')}}</small>
                            </div>
                            <div class="col-6 py-2 text-right">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_cod" id="enable_cod" {{ $store_settings['enable_cod'] == 'on' ? 'checked="checked"' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_cod">{{__('Enable COD')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-6 py-2">
                                <h5 class="h5">{{__('Stripe')}}</h5>
                                <small> {{__('Note: This detail will use for make checkout of shopping cart.')}}</small>
                            </div>
                            <div class="col-6 py-2 text-right">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="is_stripe_enabled" id="enable_stripe" {{ $store_settings['is_stripe_enabled'] == 'on' ? 'checked="checked"' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_stripe">{{__('Enable Stripe')}}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{Form::label('stripe_key',__('Stripe Key')) }}
                                    {{Form::text('stripe_key',$store_settings['stripe_key'],['class'=>'form-control','placeholder'=>__('Enter Stripe Key')])}}
                                    @error('stripe_key')
                                    <span class="invalid-stripe_key" role="alert">
                                             <strong class="text-danger">{{ $message }}</strong>
                                         </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{Form::label('stripe_secret',__('Stripe Secret')) }}
                                    {{Form::text('stripe_secret',$store_settings['stripe_secret'],['class'=>'form-control ','placeholder'=>__('Enter Stripe Secret')])}}
                                    @error('stripe_secret')
                                    <span class="invalid-stripe_secret" role="alert">
                                             <strong class="text-danger">{{ $message }}</strong>
                                         </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-6 py-2">
                                <h5 class="h5">{{__('PayPal')}}</h5>
                                <small> {{__('Note: This detail will use for make checkout of shopping cart.')}}</small>
                            </div>
                            <div class="col-6 py-2 text-right">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="is_paypal_enabled" id="is_paypal_enabled" {{ $store_settings['is_paypal_enabled'] == 'on' ? 'checked="checked"' : '' }}>
                                    <label class="custom-control-label form-control-label" for="is_paypal_enabled">{{__('Enable Paypal')}}</label>
                                </div>
                            </div>
                            <div class="col-md-12 pb-4">
                                <label class="paypal-label form-control-label" for="paypal_mode">{{__('Paypal Mode')}}</label> <br>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-primary btn-sm active">
                                        <input type="radio" name="paypal_mode" value="sandbox" {{ $store_settings['PAYPAL_MODE'] == '' || $store_settings['PAYPAL_MODE'] == 'sandbox' ? 'checked="checked"' : '' }}>{{__('Sandbox')}}
                                    </label>
                                    <label class="btn btn-primary btn-sm ">
                                        <input type="radio" name="paypal_mode" value="live" {{ $store_settings['PAYPAL_MODE'] == 'live' ? 'checked="checked"' : '' }}>{{__('Live')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="paypal_client_id">{{ __('Client ID') }}</label>
                                    <input type="text" name="paypal_client_id" id="paypal_client_id" class="form-control" value="{{$store_settings['paypal_client_id']}}" placeholder="{{ __('Client ID') }}"/>
                                    @if ($errors->has('paypal_client_id'))
                                        <span class="invalid-feedback d-block">
                                            {{ $errors->first('paypal_client_id') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="paypal_secret_key">{{ __('Secret Key') }}</label>
                                    <input type="text" name="paypal_secret_key" id="paypal_secret_key" class="form-control" value="{{$store_settings['paypal_secret_key']}}" placeholder="{{ __('Secret Key') }}"/>
                                    @if ($errors->has('paypal_secret_key'))
                                        <span class="invalid-feedback d-block">
                                            {{ $errors->first('paypal_secret_key') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-6 py-2">
                                <h5 class="h5 mb-0">{{__('Whatsapp')}}</h5>
                                <small> {{__('Note: This detail will use for make checkout of shopping cart.')}}</small>
                            </div>
                            <div class="col-6 py-2 text-right">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_whatsapp" id="enable_whatsapp" {{ $store_settings['enable_whatsapp'] == 'on' ? 'checked="checked"' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_whatsapp">{{__('Enable Whatsapp')}}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="whatsapp_number" id="whatsapp_number" class="form-control" value="{{$store_settings['whatsapp_number']}}" placeholder="{{ __('Whatsapp Number') }}"/>
                                    @if ($errors->has('whatsapp_number'))
                                        <span class="invalid-feedback d-block">
                                            {{ $errors->first('whatsapp_number') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-6 py-2">
                                <h5 class="h5 mb-0">{{__('Bank Transfer')}}</h5>
                                <small> {{__('Note: Input your bank details including bank name.')}}</small>
                            </div>
                            <div class="col-6 py-2 text-right">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="enable_bank" id="enable_bank" {{ $store_settings['enable_bank'] == 'on' ? 'checked="checked"' : '' }}>
                                    <label class="custom-control-label form-control-label" for="enable_bank">{{__('Enable Bank Transfer')}}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea type="text" name="bank_number" id="bank_number" class="form-control" value="" placeholder="{{ __('Bank Transfer Number') }}">{{$store_settings['bank_number']}}   </textarea>
                                    @if ($errors->has('bank_number'))
                                        <span class="invalid-feedback d-block">
                                            {{ $errors->first('bank_number') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {{Form::submit(__('Save Change'),array('class'=>'btn btn-sm btn-primary rounded-pill'))}}
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
                <div class="tab-pane fade show" id="email_setting" role="tabpanel" aria-labelledby="orders-tab">
                    {{Form::open(array('route'=>array('owner.email.setting',$store_settings->slug),'method'=>'post'))}}
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{Form::label('mail_driver',__('Mail Driver')) }}
                                {{Form::text('mail_driver',$store_settings->mail_driver,array('class'=>'form-control','placeholder'=>__('Enter Mail Driver')))}}
                                @error('mail_driver')
                                <span class="invalid-mail_driver" role="alert">
                                     <strong class="text-danger">{{ $message }}</strong>
                                     </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_host',__('Mail Host')) }}
                                {{Form::text('mail_host',$store_settings->mail_host,array('class'=>'form-control ','placeholder'=>__('Enter Mail Driver')))}}
                                @error('mail_host')
                                <span class="invalid-mail_driver" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_port',__('Mail Port')) }}
                                {{Form::text('mail_port',$store_settings->mail_port,array('class'=>'form-control','placeholder'=>__('Enter Mail Port')))}}
                                @error('mail_port')
                                <span class="invalid-mail_port" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_username',__('Mail Username')) }}
                                {{Form::text('mail_username',$store_settings->mail_username,array('class'=>'form-control','placeholder'=>__('Enter Mail Username')))}}
                                @error('mail_username')
                                <span class="invalid-mail_username" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_password',__('Mail Password')) }}
                                {{Form::text('mail_password',$store_settings->mail_password,array('class'=>'form-control','placeholder'=>__('Enter Mail Password')))}}
                                @error('mail_password')
                                <span class="invalid-mail_password" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_encryption',__('Mail Encryption')) }}
                                {{Form::text('mail_encryption',$store_settings->mail_encryption,array('class'=>'form-control','placeholder'=>__('Enter Mail Encryption')))}}
                                @error('mail_encryption')
                                <span class="invalid-mail_encryption" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_from_address',__('Mail From Address')) }}
                                {{Form::text('mail_from_address',$store_settings->mail_from_address,array('class'=>'form-control','placeholder'=>__('Enter Mail From Address')))}}
                                @error('mail_from_address')
                                <span class="invalid-mail_from_address" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('mail_from_name',__('Mail From Name')) }}
                                {{Form::text('mail_from_name',$store_settings->mail_from_name,array('class'=>'form-control','placeholder'=>__('Enter Mail Encryption')))}}
                                @error('mail_from_name')
                                <span class="invalid-mail_from_name" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <a href="#" data-url="{{route('test.mail' )}}" data-ajax-popup="true" data-title="{{__('Send Test Mail')}}" class="btn btn-sm btn-info rounded-pill">
                                    {{__('Send Test Mail')}}
                                </a>
                            </div>
                            <div class="form-group col-md-6 text-right">
                                {{Form::submit(__('Save Change'),array('class'=>'btn btn-sm btn-primary rounded-pill'))}}
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-page')
    <script>
        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            show_toastr('success', 'Link copied', 'success');
        }
    </script>
@endpush
