@extends('layouts.storefront')
@section('page-title')
    {{__('Cart')}}
@endsection
@push('css-page')
@endpush
@section('content')
<style>
    .table tr, .table td, .table th{
        border: none;
    }
    tr.member_product th, tr.member_product td {
        padding-top: 0 !important;
    }
    tr.member_name td{
        color: black;
        font-weight: bold;
    }
    div.imgDiv{
        padding-left:4%;
    }
</style>
    @php
        $cart = session()->get($store->slug);
    @endphp
    @if(!empty($cart['products']) || $cart['products'] = [])
        <div class="container">
            <!-- Shopping cart table -->
            <div class="table-responsive">
                <table class="table  align-items-center">
                    <thead>
                    <tr>
                        <th scope="col" class="sort text-dark" data-sort="product">{{__('Product')}}</th>
                        <th scope="col" class="sort text-dark" data-sort="price">{{__('Price')}}</th>
                        <th scope="col" class="text-dark">{{__('Quantity')}}</th>
                        <th scope="col" class="sort text-dark" data-sort="Tax">{{__('Tax')}}</th>
                        <th scope="col" class="sort text-dark" data-sort="Total">{{__('Total')}}</th>
                        <th scope="col" class="text-right text-dark">{{__('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody class="list">
                    @if(!empty($products))
                        @php
                            $sub_tax = 0;
                            $total = 0;
                        @endphp
                        @foreach($products['products'] as $team_products)
                            @foreach($team_products as $key => $product)
                                @if($product['variant_id'] != 0 )
                                    <tr class="member_name">
                                        <td colspan="6" >{{$product['team_member']}}</td>
                                    </tr>
                                    <tr data-id="{{$key}}" class="member_product">
                                        <th scope="row">
                                            <div class="media align-items-center imgDiv">

                                                <img alt="Image placeholder" src="{{asset($product['image'])}}" style="width: 80px;">

                                                <div class="media-body pl-3">
                                                    <div class="lh-100">
                                                        <span class="text-dark font-weight-bold mb-0">{{$product['product_name'].' - ( ' . $product['variant_name'] .' ) '}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <td class="price">
                                            {{\App\Utility::priceFormat($product['variant_price'])}}
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control form-control-sm text-center product_qty" data-id="{{$product['id']}}" id="product_qty" style="width: 80px;" value="{{$product['quantity']}}">
                                            <input type="hidden" class="pro_variant_id" data-id="{{$product['variant_id']}}">
                                        </td>
                                        <td class="total">
                                            @php
                                                $total_tax=0;
                                            @endphp
                                            @if(!empty($product['tax']))
                                                @foreach($product['tax'] as $tax)
                                                    @php
                                                        $sub_tax = ($product['variant_price']* $product['quantity'] * $tax['tax']) / 100;
                                                        $total_tax += $sub_tax;
                                                    @endphp
                                                    <span class="btn badge badge-dark text-xs small">
                                                            {{$tax['tax_name'].' '.$tax['tax'].'%'.' ('.$sub_tax.')'}}
                                                        </span>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="total">
                                            @php
                                                $totalprice = $product['variant_price'] * $product['quantity'] + $total_tax;
                                                $total += $totalprice;
                                            @endphp
                                            {{\App\Utility::priceFormat($totalprice)}}

                                        </td>
                                        <td class="text-right">
                                            <!-- Actions -->
                                            <div class="actions ml-3">
                                                <a href="#!" class="action-item mr-2" data-toggle="tooltip" data-original-title="{{__('Move to trash')}}" data-confirm="{{__('Are You Sure?').' | '.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-product-cart-{{$key}}').submit();">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['delete.cart_item',[$store->slug,$product['product_id'],$product['team_member'],$product['variant_id']]],'id'=>'delete-product-cart-'.$key]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="table-divider"></tr>
                                @else
                                    <tr class="member_name">
                                        <td colspan="6">{{$product['team_member']}}</td>
                                    </tr>
                                    <tr data-id="{{$key}}" class="member_product">
                                        <th scope="row">
                                            <div class="media align-items-center imgDiv">

                                                <img alt="Image placeholder" src="{{asset($product['image'])}}" style="width: 80px;">

                                                <div class="media-body pl-3">
                                                    <div class="lh-100">
                                                        <span class="text-dark font-weight-bold mb-0">{{$product['product_name']}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <td class="price">
                                            {{\App\Utility::priceFormat($product['price'])}}
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control form-control-sm text-center product_qty" data-id="{{$product['id']}}" id="product_qty" style="width: 80px;" value="{{$product['quantity']}}">
                                        </td>
                                        <td class="total">
                                            @php
                                                $total_tax=0;
                                            @endphp
                                            @if($product['tax'] > 0)
                                                @foreach($product['tax'] as $tax)
                                                    @php
                                                        $sub_tax = ($product['price']* $product['quantity'] * $tax['tax']) / 100;
                                                        $total_tax += $sub_tax;
                                                    @endphp
                                                    <span class="btn badge badge-dark text-xs small">
                                            {{$tax['tax_name'].' '.$tax['tax'].'%'.' ('.$sub_tax.')'}}
                                            </span>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="total">
                                            @php
                                                $totalprice = $product['price'] * $product['quantity'] + $total_tax;
                                                $total += $totalprice;
                                            @endphp
                                            {{\App\Utility::priceFormat($totalprice)}}

                                        </td>
                                        <td class="text-right">
                                            <!-- Actions -->
                                            <div class="actions ml-3">
                                                <a href="#!" class="action-item mr-2" data-toggle="tooltip" data-original-title="{{__('Move to trash')}}" data-confirm="{{__('Are You Sure?').' | '.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-product-cart-{{$key}}').submit();">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['delete.cart_item',[$store->slug,$product['product_id'],$product['team_member']]],'id'=>'delete-product-cart-'.$key]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="table-divider"></tr>
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- Cart information -->
            <div class="card mt-5 bg-white">
                <div class="card-body">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6 order-md-2 mb-4 mb-md-0">
                            <div class="d-flex align-items-center justify-content-md-end">
                                <span class="h6 text-muted d-inline-block mr-3 mb-0">{{__('Total value')}}:</span>
                                <span class="h4 mb-0">
                                {{\App\Utility::priceFormat(!empty($total)?$total:0)}}
                            </span>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1">
                            <button type="button" class="btn btn-animated btn-dark btn-animated-y">
                                <span class="btn-inner--visible">{{__('Checkout')}}</span>
                                <a href="{{route('user-address.useraddress',$store->slug)}}">
                                <span class="btn-inner--hidden">
                                    <i class="fas fa-shopping-cart text-white"></i>
                                </span>
                                </a>
                            </button>
                            <a href="{{route('store.slug',$store->slug)}}" class="btn btn-link text-sm text-dark font-weight-bold">{{__('Return to shop')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="main-content">
            <section class="mh-100vh d-flex align-items-center" data-offset-top="#header-main">
                <!-- SVG background -->
                <div class="bg-absolute-cover bg-size--contain d-flex align-items-center zindex0">
                    <figure class="w-100 px-4">
                        <img alt="Image placeholder" src="{{asset('assets/img/bg-3.svg')}}" class="svg-inject">
                    </figure>
                </div>
                <div class="container pt-6 position-relative">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <div class="text-center">
                                <!-- SVG illustration -->
                                <div class="row justify-content-center mb-5">
                                    <div class="col-md-5">
                                        <img alt="Image placeholder" src="{{asset('assets/img/online-shopping.svg')}}" class="svg-inject img-fluid">
                                    </div>
                                </div>
                                <!-- Empty cart container -->
                                <h6 class="h4 my-4">{{__('Your cart is empty')}}.</h6>
                                <p class="px-md-5">
                                    {{__('Your cart is currently empty. Return to our shop and check out the latest offers.
                                    We have some great items that are waiting for you')}}.
                                </p>
                                <a href="{{route('store.slug',$store->slug)}}" class="btn btn-sm btn-primary btn-icon rounded-pill my-5">
                                    <span class="btn-inner--icon"><i class="fas fa-angle-left"></i></span>
                                    <span class="btn-inner--text">{{__('Return to shop')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    @endif
@endsection
@push('script-page')
    <script>
        $(".product_qty").on('change keyup', function (e) {
            e.preventDefault();
            var product_id = $(this).attr('data-id');
            var arrkey = $(this).parents('tr').attr('data-id');
            var qty_id = $(this).val();

            if (qty_id == 0 || qty_id == '' || qty_id < 0) {

                location.reload();
                return false
            }
            $.ajax({
                url: '{{route('user-product_qty.product_qty',['__product_id',$store->slug,'arrkeys'])}}'.replace('__product_id', product_id).replace('arrkeys', arrkey),
                type: "post",
                headers: {
                    'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "product_qty": qty_id,
                },
                success: function (response) {
                    if (response.status == "Error") {
                        show_toastr('Error', response.error, 'error');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {
                        location.reload(); // then reload the page.(3)
                    }
                },
                error: function (result) {
                    console.log('error12');
                }
            });
        })
    </script>
@endpush
