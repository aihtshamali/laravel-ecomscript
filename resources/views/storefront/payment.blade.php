@extends('layouts.storefront')
@section('page-title')
    {{__('Payment')}}
@endsection
@push('script-page')
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        var stripe = Stripe('{{ $store->stripe_key }}');
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        var style = {
            base: {
                // Add your base input styles here. For example:
                fontSize: '14px',
                color: '#32325d',
            },
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Create a token or display an error when the form is submitted.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    $("#card-errors").html(result.error.message);
                    show_toastr('Error', result.error.message, 'error');
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            form.submit();
        }
    </script>
    <script>
        $(document).on('click', '#owner-whatsapp', function () {
            var product_array = '{{$encode_product}}';
            var product = JSON.parse(product_array.replace(/&quot;/g, '"'));
            var order_id = '{{$order_id = '#'.str_pad(!empty($order->id) ? $order->id + 1 : 0 + 1, 4, "100", STR_PAD_LEFT)}}';

            var data = {
                product: product,
                order_id: order_id,
                wts_number: $('#wts_number').val()
            }
            $.ajax({
                url: '{{ route('user.whatsapp',$store->slug) }}',
                method: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 'success') {

                        show_toastr(data["success"], '{!! session('+data["status"]+') !!}', data["status"]);

                        setTimeout(function () {
                            var url = '{{ route('store-complete.complete', [$store->slug, ":id"]) }}';
                            url = url.replace(':id', data.order_id);

                            window.location.href = url;
                        }, 1000);

                        var append_href = '{!! $url !!}' + '{{route('user.order',[$store->slug,Crypt::encrypt(!empty($order->id) ? $order->id + 1 : 0 + 1)])}}';
                        window.open(append_href, '_blank');

                    } else {
                        show_toastr("Error", data.success, data["status"]);
                    }
                }
            });
        });
        $(document).on('click', '#cash_on_delivery', function () {

            var product_array = '{{$encode_product}}';
            var product = JSON.parse(product_array.replace(/&quot;/g, '"'));
            var order_id = '{{$order_id = '#'.str_pad(!empty($order->id) ? $order->id + 1 : 0 + 1, 4, "100", STR_PAD_LEFT)}}';

            var data = {
                product: product,
                order_id: order_id,
            }
            $.ajax({
                url: '{{ route('user.cod',$store->slug) }}',
                method: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 'success') {

                        show_toastr(data["success"], '{!! session('+data["status"]+') !!}', data["status"]);

                        setTimeout(function () {
                            var url = '{{ route('store-complete.complete', [$store->slug, ":id"]) }}';
                            url = url.replace(':id', data.order_id);
                            window.location.href = url;
                        }, 1000);

                    } else {
                        show_toastr("Error", data.success, data["status"]);
                    }
                }
            });
        });
        $(document).on('click', '#bank_transfer', function () {
            var product_array = '{{$encode_product}}';
            var product = JSON.parse(product_array.replace(/&quot;/g, '"'));
            var order_id = '{{$order_id = '#'.str_pad(!empty($order->id) ? $order->id + 1 : 0 + 1, 4, "100", STR_PAD_LEFT)}}';

            var data = {
                product: product,
                order_id: order_id,
            }
            $.ajax({
                url: '{{ route('user.bank_transfer',$store->slug) }}',
                method: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 'success') {

                        show_toastr(data["success"], '{!! session('+data["status"]+') !!}', data["status"]);

                        setTimeout(function () {
                            var url = '{{ route('store-complete.complete', [$store->slug, ":id"]) }}';
                            url = url.replace(':id', data.order_id);

                            window.location.href = url;
                        }, 1000);

                    } else {
                        show_toastr("Error", data.success, data["status"]);
                    }
                }
            });
        });
    </script>
@endpush
@section('content')
    <div class="container">
        <div class="row row-grid">
            <div class="col-lg-8">
                <!-- Add money using COD -->
                @if($store['enable_cod'] == 'on')
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8">
                                    <div>
                                        <i class="fas fa-shipping-fast"></i>
                                        <label class="h6 lh-180" for="radio-payment-paypal">{{__('Cash On Delivery')}}</label>
                                    </div>
                                    <label class="">{{__('Cash on delivery is a type of transaction in which payment for a good is made at the time of delivery')}}.</label>
                                </div>
                                <div class="col-4 text-right">
                                    <img alt="Image placeholder" src="{{asset('assets/img/cod.png')}}" width="70">
                                    <form class="w3-container w3-display-middle w3-card-4" method="POST" action="{{ route('user.cod',$store->slug) }}">
                                        @csrf
                                        <input type="hidden" name="product_id">
                                        <div class="form-group mt-3">
                                            <div class="text-sm-right ">
                                                <button type="button" class="btn btn-dark btn-sm float-right" id="cash_on_delivery">{{__('Complete Order')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endif
            <!-- Add money using Stripe -->
                @if($store['is_stripe_enabled'] == 'on')
                    <form role="form" action="{{ route('stripe.post',$store->slug) }}" method="post" class="require-validation" id="payment-form">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-8">
                                        <div>
                                            <label class="h6 mb-0 lh-180" for="radio-payment-card">{{__('Credit Card (Strip)')}}</label>
                                        </div>
                                        <p class="text-muted mt-2 mb-0">{{__('Safe money transfer using your bank account. We support Mastercard, Visa, Maestro and Skrill')}}.</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <img alt="Image placeholder" src="{{asset('assets/img/mastercard.png')}}" width="40" class="mr-2">
                                        <img alt="Image placeholder" src="{{asset('assets/img/visa.png')}}" width="40" class="mr-2">
                                        <img alt="Image placeholder" src="{{asset('assets/img/skrill.png')}}" width="40">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="card-name-on">{{__('Name on card')}}</label>
                                            <input type="text" name="name" id="card-name-on" class="form-control required" placeholder="Enter Your Name">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="card-element"></div>
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                    <div class="col-md-10">
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="error" style="display: none;">
                                            <div class='alert-danger alert'>{{__('Please correct the errors and try again.')}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="text-sm-right mr-2">
                                                <input type="hidden" name="plan_id">
                                                <button class="btn btn-dark btn-sm" type="submit">
                                                    <i class="mdi mdi-cash-multiple mr-1"></i> {{__('Pay Now')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endif
            <!-- Add money using PayPal -->
                @if($store['is_paypal_enabled'] == 'on')
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8">
                                    <div>
                                        <label class="h6 mb-0 lh-180" for="radio-payment-paypal">{{__('PayPal')}}</label>
                                    </div>
                                    <p class="text-sm text-muted mt-2 mb-0">
                                        {{__('Pay your order using the most known and secure platform for online money transfers. You will be redirected to PayPal to finish complete your purchase')}}.
                                    </p>
                                </div>
                                <div class="col-4 text-right">
                                    <img alt="Image placeholder" src="{{asset('assets/img/paypal-256x160.png')}}" width="70">
                                    <form class="w3-container w3-display-middle w3-card-4" method="POST" action="{{ route('pay.with.paypal',$store->slug) }}">
                                        @csrf
                                        <input type="hidden" name="product_id">
                                        <div class="form-group mt-3">
                                            <div class="text-sm-right ">
                                                <button class="btn btn-dark btn-sm float-right" type="submit">
                                                    <i class="mdi mdi-cash-multiple mr-1"></i> {{__('Pay Now')}}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endif
            <!-- Add money using whatsapp -->
                @if($store['enable_whatsapp'] == 'on')
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8">
                                    <div>
                                        <i class="fab fa-whatsapp"></i>
                                        <label class="h6 lh-180" for="radio-payment-paypal">{{__('Whatsapp')}}</label>
                                    </div>
                                    <label>{{__('Click to chat. The click to chat feature lets customers click an URL in order to directly start a chat with another person or business via WhatsApp. ...')}}</label>
                                    <label>{{__('QR code. As you know, having to add a phone number to your contacts in order to start up a WhatsApp message can take a little while. ...')}}.</label>
                                    <input type="text" name="wts_number" id="wts_number" class="form-control input-mask mt-2" data-mask="+00 0000000000" placeholder="Enter Your Phone Number">
                                </div>
                                <div class="col-4 text-right">
                                    <img alt="Image placeholder" src="{{asset('assets/img/whatsapp.png')}}" width="70">
                                    <form class="w3-container w3-display-middle w3-card-4" method="POST" action="{{ route('user.whatsapp',$store->slug) }}">
                                        @csrf
                                        <input type="hidden" name="product_id">
                                        <div class="form-group mt-3">
                                            <div class="text-sm-right ">
                                                <button type="button" class="btn btn-dark btn-sm float-right" id="owner-whatsapp">{{__('Complete Order')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endif
            <!-- Add money using Bank Transfer -->
                @if($store['enable_bank'] == 'on')
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8">
                                    <div>
                                        <i class="fas fa-university"></i>
                                        <label class="h6 lh-180" for="bank-transfer">{{__('Bank Transfer')}}</label>
                                    </div>
                                    <p class="white_space">{{$store->bank_number}}</p>
                                </div>
                                <div class="col-4 text-right">
                                    <img alt="Image placeholder" src="{{asset('assets/img/bank.png')}}" width="70">
                                    <form class="w3-container w3-display-middle w3-card-4" method="POST" action="{{ route('user.bank_transfer',$store->slug) }}">
                                        @csrf
                                        <input type="hidden" name="product_id">
                                        <div class="form-group mt-3">
                                            <div class="text-sm-right ">
                                                <button type="button" class="btn btn-dark btn-sm float-right" id="bank_transfer">{{__('Complete Order')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="mt-4 text-right">
                    <a href="{{route('store.slug',$store->slug)}}" class="btn bg-white-light btn-link text-sm text-dark font-weight-bold">{{__('Return to shop')}}</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div data-toggle="sticky" data-sticky-offset="30">
                    <div class="card" id="card-summary">
                        <div class="card-header py-3">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <span class="h6">{{__('Summary')}}</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span class="badge badge-pill badge-soft-success">{{$total_item}} items</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if(!empty($products))
                                @php
                                    $total = 0;
                                    $sub_tax = 0;
                                    $sub_total= 0;
                                @endphp
                                @foreach($products as $team_products)
                                    @foreach($team_products as $product)
                                        @if($product['variant_id'] !=0)
                                            <strong>{{$product['team_member']}}</strong>
                                            <div class="row mb-2 pb-2 delimiter-bottom">
                                                <div class="col-8">
                                                    <div class="media align-items-center">
                                                        <img alt="Image placeholder" class="mr-2" src="{{asset($product['image'])}}" style="width: 42px;">
                                                        <div class="media-body">
                                                            <div class="text-limit lh-100">
                                                                <small class="font-weight-bold mb-0">{{$product['product_name'].' - ( ' . $product['variant_name'] .' ) '}}</small>
                                                            </div>
                                                            @php
                                                                $total_tax=0;
                                                            @endphp
                                                            <small class="text-muted">{{$product['quantity']}} x {{\App\Utility::priceFormat($product['variant_price'])}}
                                                                @if(!empty($product['tax']))
                                                                    +
                                                                    @foreach($product['tax'] as $tax)
                                                                        @php
                                                                            $sub_tax = ($product['variant_price'] * $product['quantity'] * $tax['tax']) / 100;
                                                                            $total_tax += $sub_tax;
                                                                        @endphp

                                                                        {{\App\Utility::priceFormat($sub_tax).' ('.$tax['tax_name'].' '.($tax['tax']).'%)'}}
                                                                    @endforeach
                                                                @endif
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 text-right lh-100">
                                                    <small class="text-dark">
                                                        @php
                                                            $totalprice = $product['variant_price'] * $product['quantity'] + $total_tax;
                                                            $subtotal = $product['variant_price'] * $product['quantity'];
                                                            $sub_total += $subtotal;
                                                        @endphp
                                                        {{\App\Utility::priceFormat($totalprice)}}
                                                    </small>
                                                    @php
                                                        $total += $totalprice;
                                                    @endphp
                                                </div>
                                            </div>
                                        @else
                                            <strong>{{$product['team_member']}}</strong>
                                            <div class="row mb-2 pb-2 delimiter-bottom">
                                                <div class="col-8">
                                                    <div class="media align-items-center">
                                                        <img alt="Image placeholder" class="mr-2" src="{{asset($product['image'])}}" style="width: 42px;">
                                                        <div class="media-body">
                                                            <div class="text-limit lh-100">
                                                                <small class="font-weight-bold mb-0">{{$product['product_name']}}</small>
                                                            </div>
                                                            @php
                                                                $total_tax=0;
                                                            @endphp
                                                            <small class="text-muted">{{$product['quantity']}} x {{\App\Utility::priceFormat($product['price'])}}
                                                                @if(!empty($product['tax']))
                                                                    +
                                                                    @foreach($product['tax'] as $tax)
                                                                        @php
                                                                            $sub_tax = ($product['price'] * $product['quantity'] * $tax['tax']) / 100;
                                                                            $total_tax += $sub_tax;
                                                                        @endphp

                                                                        {{\App\Utility::priceFormat($sub_tax).' ('.$tax['tax_name'].' '.$tax['tax'].'%)'}}
                                                                    @endforeach
                                                                @endif
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 text-right lh-100">
                                                    <small class="text-dark">
                                                        @php
                                                            $totalprice = $product['price'] * $product['quantity'] + $total_tax;
                                                            $subtotal = $product['price'] * $product['quantity'];
                                                            $sub_total += $subtotal;
                                                        @endphp
                                                        {{\App\Utility::priceFormat($totalprice)}}
                                                    </small>
                                                    @php($total += $totalprice)
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                        <!-- Subtotal -->
                            <div class="row mt-2 pt-2  pb-3">
                                <div class="col-8 text-right">
                                    <small class="font-weight-bold">{{__('Subtotal (Before Tax)')}}:</small>
                                </div>
                                <div class="col-4 text-right">
                                    <span class="text-sm font-weight-bold"> {{\App\Utility::priceFormat(!empty($sub_total)?$sub_total:0)}}</span>

                                </div>
                            </div>
                            <!-- Shipping -->
                            @foreach($taxArr['tax'] as $k=>$tax)
                                <div class="row mt-2 pt-2 border-top">
                                    <div class="col-8 text-right">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <div class="text-limit lh-100">
                                                    <small class="font-weight-bold mb-0">{{$tax}}</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <span class="text-sm font-weight-bold">{{\App\Utility::priceFormat($taxArr['rate'][$k])}}</span>
                                    </div>
                                </div>
                            @endforeach
                            @if($store->enable_shipping == 'on')
                                <div class="shipping_price_add">
                                    <div class="row mt-3 pt-3 border-top">
                                        <div class="col-8 text-right pt-2">
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <div class="text-limit lh-100 text-sm">{{__('Shipping Price')}}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 text-right"><span class="text-sm font-weight-bold shipping_price">{{\App\Utility::priceFormat(!empty($shipping_price)?$shipping_price:0)}}</span></div>
                                    </div>
                                </div>
                        @endif
                        <!-- Subtotal -->
                            <div class="row mt-3 pt-3 border-top">
                                <div class="col-8 text-right">
                                    <small class="text-uppercase font-weight-bold">{{__('Total')}}:</small>
                                </div>
                                <div class="col-4 text-right">
                                    @if(!empty($shipping_price) && $shipping_price != 0)
                                        <span class="text-sm font-weight-bold"> {{\App\Utility::priceFormat(!empty($total)?$total+$shipping_price:0)}}</span>
                                    @else
                                        <span class="text-sm font-weight-bold"> {{\App\Utility::priceFormat(!empty($total)?$total:0)}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-page')
    <script src="{{asset('assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
@endpush
