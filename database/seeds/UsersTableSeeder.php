<?php

use App\User;
use App\Store;
use App\UserStore;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create(
            [
                'name' => 'Owner',
                'email' => 'owner@example.com',
                'password' => Hash::make('1234'),
                'type' => 'Owner',
                'lang' => 'en',
                'created_by' => 0,
            ]
        );

        $objStore             = Store::create(
            [
                'name' => 'My Store',
                'email' => 'mystore@example.com',
                'created_by' => $admin->id,
                'whatsapp' => '#',
                'facebook' => '#',
                'instagram' => '#',
                'twitter' => '#',
                'youtube' => '#',
                'enable_header_img' => 'on',
                'header_img' => 'img-15.jpg',
                'header_title' => 'Home Accessories',
                'header_desc' => 'There is only that moment and the incredible certainty that everything under the sun has been written by one hand only.',
                'button_text' => 'Start shopping',
                'enable_rating' => 'on',
                'enable_subscriber' => 'on',
                'sub_img' => 'img-17.jpg',
                'subscriber_title' => 'Always on time',
                'sub_title' => 'Subscription here',
                'footer_note' => '©StoreGo',
                'logo' => 'logo.png',

            ]
        );
        $admin->current_store = $objStore->id;
        $admin->save();

        UserStore::create(
            [
                'user_id' => $admin->id,
                'store_id' => $objStore->id,
                'permission' => 'Owner',
            ]
        );

    }
}
