<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_id',
        'name',
        'email',
        'card_number',
        'card_exp_month',
        'card_exp_year',
        'user_address_id',
        'product_id',
        'price',
        'product',
        'price_currency',
        'txn_id',
        'payment_type',
        'payment_status',
        'status',
        'phone',
        'receipt',
        'user_id',
    ];
}
