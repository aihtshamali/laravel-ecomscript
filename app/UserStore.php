<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStore extends Model
{
    protected $fillable = [
        'user_id',
        'store_id',
        'permission',
        'is_active',
    ];
}
