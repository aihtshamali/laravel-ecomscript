<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/login/{lang?}', 'Auth\LoginController@showLoginForm')->name('login');

Route::get('/password/resets/{lang?}', 'Auth\LoginController@showLinkRequestForm')->name('change.langPass');

Route::get(
    '/', [
           'as' => 'dashboard',
           'uses' => 'DashboardController@index',
       ]
)->middleware(
    [
        'XSS',
    ]
);
Route::get(
    '/dashboard', [
                    'as' => 'dashboard',
                    'uses' => 'DashboardController@index',
                ]
)->middleware(
    [

        'XSS',
        'auth',
    ]
);

Route::group(['middleware' => ['auth', 'XSS']], function (){
    Route::get('change-language/{lang}', 'LanguageController@changeLanquage')->name('change.language')->middleware(['auth', 'XSS']);
    Route::get('manage-language/{lang}', 'LanguageController@manageLanguage')->name('manage.language')->middleware(['auth', 'XSS']);
    Route::post('store-language-data/{lang}', 'LanguageController@storeLanguageData')->name('store.language.data')->middleware(['auth', 'XSS']);
    Route::get('create-language', 'LanguageController@createLanguage')->name('create.language')->middleware(['auth', 'XSS']);
    Route::post('store-language', 'LanguageController@storeLanguage')->name('store.language')->middleware(['auth', 'XSS']);

    Route::delete('/lang/{lang}', 'LanguageController@destroyLang')->name('lang.destroy')->middleware(['auth', 'XSS']);
});

Route::group(
    [
        'middleware' => [
            'auth',
        ],
    ], function (){
    Route::resource('stores', 'StoreController');
    Route::post('store-setting/{id}', 'StoreController@savestoresetting')->name('settings.store');
}
);
Route::get('change-language-store/{slug?}/{lang}', 'LanguageController@changeLanquageStore')->name('change.languagestore')->middleware(['XSS']);
Route::get('/change/mode',['as' => 'change.mode','uses' =>'DashboardController@changeMode']);

Route::get('profile', 'DashboardController@profile')->name('profile')->middleware(
    [
        'auth',
        'XSS',
    ]
);
Route::put('change-password', 'DashboardController@updatePassword')->name('update.password');
Route::put('edit-profile', 'DashboardController@editprofile')->name('update.account')->middleware(
    [
        'auth',
        'XSS',
    ]
);


Route::get('storeanalytic', 'StoreAnalytic@index')->middleware('auth')->name('storeanalytic')->middleware(['XSS']);

Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::post('business-setting', 'SettingController@saveBusinessSettings')->name('business.setting');
    Route::post('company-setting', 'SettingController@saveCompanySettings')->name('company.setting');
    Route::post('email-setting', 'SettingController@saveEmailSettings')->name('email.setting');
    Route::post('system-setting', 'SettingController@saveSystemSettings')->name('system.setting');
    Route::post('pusher-setting', 'SettingController@savePusherSettings')->name('pusher.setting');
    Route::get('test-mail', 'SettingController@testMail')->name('test.mail');
    Route::post('test-mail', 'SettingController@testSendMail')->name('test.send.mail');
    Route::get('settings', 'SettingController@index')->name('settings');
    Route::post('payment-setting', 'SettingController@savePaymentSettings')->name('payment.setting');
    Route::post('owner-payment-setting/{id}', 'SettingController@saveOwnerPaymentSettings')->name('owner.payment.setting');
    Route::post('owner-email-setting/{slug?}', 'SettingController@saveOwneremailSettings')->name('owner.email.setting');
}
);
Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::resource('product_categorie', 'ProductCategorieController');
}
);
Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::resource('product_tax', 'ProductTaxController');
}
);
Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::resource('shipping', 'ShippingController');
}
);
Route::resource('location', 'LocationController')->middleware(['auth', 'XSS']);
Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::resource('orders', 'OrderController');
}
);
Route::group(
    [
        'middleware' => [
            'XSS',
        ],
    ], function (){
    Route::resource('rating', 'RattingController');
    Route::post('rating_view', 'RattingController@rating_view')->name('rating.rating_view');
    Route::get('rating/{slug?}/product/{id}', 'RattingController@rating')->name('rating');
    Route::post('stor_rating/{slug?}/product/{id}', 'RattingController@stor_rating')->name('stor_rating');
    Route::post('edit_rating/product/{id}', 'RattingController@edit_rating')->name('edit_rating');
}
);
Route::group(
    [
        'middleware' => [
            'XSS',
        ],
    ], function (){
    Route::resource('subscriptions', 'SubscriptionController');
    Route::POST('subscriptions/{id}', 'SubscriptionController@store_email')->name('subscriptions.store_email');
}
);
Route::group(
    [
        'middleware' => [
            'auth',
            'XSS',
        ],
    ], function (){
    Route::get('/product-variants/create',['as' => 'product.variants.create','uses' =>'ProductController@productVariantsCreate']);
    Route::get('/get-product-variants-possibilities',['as' => 'get.product.variants.possibilities','uses' =>'ProductController@getProductVariantsPossibilities']);
    Route::get('product/grid', 'ProductController@grid')->name('product.grid');
    Route::resource('product', 'ProductController');
    Route::delete('product/{id}/delete', 'ProductController@fileDelete')->name('products.file.delete');
    Route::post('product/{id}/update', 'ProductController@productUpdate')->name('products.update');
}
);
Route::get('get-products-variant-quantity',['as' => 'get.products.variant.quantity','uses' =>'ProductController@getProductsVariantQuantity']);
Route::group(
    [
        'middleware' => [
            'auth',
        ],
    ], function (){
    //    Route::get('store-grid/grid', 'StoreController@grid')->name('store-grid.grid');
    Route::resource('store-resource', 'StoreController');
}
);
Route::get('/store-change/{id}','StoreController@changeCurrantStore')->name('change_store')->middleware(['auth','XSS']);


Route::get('store/{slug?}', 'StoreController@storeSlug')->name('store.slug');
Route::get('user-cart-item/{slug?}/cart', 'StoreController@StoreCart')->name('store.cart');
Route::get('user-address/{slug?}/useraddress', 'StoreController@userAddress')->name('user-address.useraddress');
Route::get('store-payment/{slug?}/userpayment', 'StoreController@userPayment')->name('store-payment.payment');
Route::get('store/{slug?}/product/{id}', 'StoreController@productView')->name('store.product.product_view');
Route::post('user-product_qty/{slug?}/product/{id}/{variant_id?}', 'StoreController@productqty')->name('user-product_qty.product_qty');
Route::post('customer/{slug}', 'StoreController@customer')->name('store.customer');
Route::post('user-location/{slug}/location/{id}', 'StoreController@UserLocation')->name('user.location');
Route::post('user-shipping/{slug}/shipping/{id}', 'StoreController@UserShipping')->name('user.shipping');
Route::post('save-rating/{slug}', 'StoreController@saverating')->name('store.saverating');
Route::delete('delete_cart_item/{slug}/product/{id}/{team_member?}/{variant_id?}', 'StoreController@delete_cart_item')->name('delete.cart_item');

Route::get('store-complete/{slug?}/{id}', 'StoreController@complete')->name('store-complete.complete');

Route::post('add-to-cart/{slug?}/{id}/{variant_id?}', 'StoreController@addToCart')->name('user.addToCart');
Route::DELETE('ownerstore-delete/{id}', 'StoreController@ownerstoredestroy')->name('ownerstore.destroy');

Route::get('getMembers/{slug}', 'StoreController@getMembers')->name('user.getMembers');
Route::post('saveMember/{slug}', 'StoreController@storeMember')->name('user.storeMember');

Route::post('payment-setting', 'SettingController@savePaymentSettings')->name('payment.setting');

Route::group(['middleware' => ['XSS']], function (){
    Route::get('order', 'StripePaymentController@index')->name('order.index');
    Route::get('/stripe/{code}', 'StripePaymentController@stripe')->name('stripe');
    Route::post('/stripe/{slug?}', 'StripePaymentController@stripePost')->name('stripe.post');
});

Route::post('pay-with-paypal/{slug?}', 'PaypalController@PayWithPaypal')->name('pay.with.paypal')->middleware(['XSS']);

Route::get('{id}/get-payment-status{slug?}', 'PaypalController@GetPaymentStatus')->name('get.payment.status')->middleware(['XSS']);

Route::get('{slug?}/order/{id}', 'StoreController@userorder')->name('user.order');

Route::post('{slug?}/whatsapp', 'StoreController@whatsapp')->name('user.whatsapp');
Route::post('{slug?}/cod', 'StoreController@cod')->name('user.cod');
Route::post('{slug?}/bank_transfer', 'StoreController@bank_transfer')->name('user.bank_transfer');

Route::get('qr-code', function () {
    return QrCode::generate();
});
