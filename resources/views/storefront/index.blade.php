<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="StoreGo - Business Ecommerce">
    <meta name="author" content="Rajodiya Infotech">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

    <title>{{'Home'}} - {{($store->tagline) ?  $store->tagline : config('APP_NAME', 'StoreGo')}}</title>

    <link rel="icon" href="{{asset(Storage::url('uploads/logo/').(!empty($settings->value)?$settings->value:'favicon.png'))}}" type="image/png">
    <link rel="stylesheet" href="{{asset('assets/libs/@fortawesome/fontawesome-free/css/all.min.css')}}"><!-- Page CSS -->
    <link rel="stylesheet" href="{{asset('assets/libs/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libs/swiper/dist/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libs/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}" id="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}" id="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" id="stylesheet')}}">
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.1.min.js')}}"></script>
</head>
<style>
    .shoping_count:after {
        content: attr(value);
        font-size: 14px;
        background: #273444;
        border-radius: 50%;
        padding: 1px 5px 1px 4px;
        position: relative;
        left: -5px;
        top: -10px;
    }   
</style>
<body>
@php
    if(!empty(session()->get('lang')))
    {
        $currantLang = session()->get('lang');
    }else{
        $currantLang = $store->lang;
    }
    $languages=\App\Utility::languages();
@endphp
<header class="header" id="header-main">
    <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark">
        <div class="container px-0">
            <div class="navbar-nav align-items-center">
                <a class="navbar-brand mr-lg-4" href="{{route('store.slug',$store->slug)}}">
                    @if(!empty($store->logo))
                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/store_logo/'.$store->logo))}}" id="navbar-logo" style="height: 40px;">
                    @else
                        <img alt="Image placeholder" src="{{asset(Storage::url('uploads/store_logo/logo.png'))}}" id="navbar-logo" style="height: 40px;">
                    @endif
                </a>
                <div class="d-none d-lg-inline-block">
                    <span class="navbar-text mr-3 pt-3 text-lg">{{ucfirst($store->name)}}</span>
                </div>
                <div class="ml-auto">
                    <ul class="nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="text-sm mb-0"><i class="fas fa-globe-asia"></i>
                                    {{Str::upper($currantLang)}}
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                @foreach($languages as $language)
                                    <a href="{{route('change.languagestore',[$store->slug,$language])}}" class="dropdown-item @if($language == $currantLang) active-language @endif">
                                        <span> {{Str::upper($language)}}</span>
                                    </a>
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('store.cart',$store->slug)}}">
                                <i class="fa badge shoping_count" id="shoping_count" style="font-size:16px" value="{{!empty($total_item)?$total_item:'0'}}">&#xf07a;</i>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-content">
    @if($store->enable_header_img == 'on')
        @if(!empty($store->header_img))
            <section class="slice slice-xl bg-cover bg-size--cover" data-offset-top="#header-main" style="background-image: url({{asset(Storage::url('uploads/store_logo/'.$store->header_img))}}); background-position: center center;">
                @else
                    <section class="slice slice-xl bg-cover bg-size--cover" data-offset-top="#header-main" style="background-image: url({{asset(Storage::url('uploads/store_logo/img-15.jpg'))}}); background-position: center center;">
                        @endif
                        <span class="mask bg-dark opacity-3"></span>
                        <div class="container py-6">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <h2 class="h1 text-white">
                                        {{!empty($store->header_title)?$store->header_title:'Home Accessories'}}
                                    </h2>
                                    <p class="lead text-white mt-4">{{!empty($store->header_desc)?$store->header_desc:'There is only that moment and the incredible certainty that everything under the sun has been written by one hand only.'}}</p>
                                    <a href="#" id="pro_scroll" class="btn btn-white rounded-pill mt-4 shadow hover-shadow-lg hover-translate-y-n3">{{!empty($store->button_text)?$store->button_text:'Start shopping'}}</a>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
                <div class="d-flex pt-4" id="pro_items">
                    <div class="btn-group btn-group-nav shadow m-auto" role="group" aria-label="Basic example">
                        <div class="btn-group nav nav-pills" role="group">
                            @foreach($categories as $key=>$category)
                                <a href="#{!!str_replace(' ','_',$category)!!}" data-id="{{$key}}" class="btn btn-white btn-icon {{($key==0)?'active':''}} productTab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="false">
                                    <span class="btn-inner--text d-inline-block">{{$category}}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    @foreach($products as $key=>$items)
                        <div class="tab-pane fade {{($key=="All")?'active show':''}}" id="{!! str_replace(' ', '_', $key)!!}" role="tabpanel" aria-labelledby="orders-tab">
                            <section class="slice slice-sm delimiter-bottom" id="sct-products">
                                <div class="container">
                                    <div class="row">
                                        @foreach($items as $product)
                                            <div class="col-lg-4 col-sm-6">
                                                <div class="card card-fluid card-product">
                                                    <div class="card-header border-bottom">
                                                        <h2 class="h6 m-0">
                                                            <a href="{{route('store.product.product_view',[$store->slug,$product->id])}}" class="float-left">
                                                                {{$product->name}}
                                                            </a>
                                                        </h2>
                                                        @if($product->enable_product_variant == 'on')
                                                            <a href="{{route('store.product.product_view',[$store->slug,$product->id])}}" class="submit-button btn btn-sm btn-danger btn-icon-only rounded-circle float-right text-white" data-modalid="{!! str_replace(' ', '_', $key)!!}_dialog_confirm_map_{{$product->id}}" data-id="{{$product->id}}" data-ajax-popup="true" data-title="Add team members">
                                                                <i class="fas fa-plus"></i>
                                                            </a>
                                                        @else
                                                        <a href="#" data-id="{{$product->id}}" class="submit-button btn btn-sm btn-danger btn-icon-only rounded-circle float-right text-white" data-size="lg" data-modalid="{!! str_replace(' ', '_', $key)!!}_dialog_confirm_map_{{$product->id}}" data-id="{{$product->id}}" data-ajax-popup="true" data-title="Add team members">
                                                            <i class="fas fa-plus"></i>
                                                        </a>                                                       
                                                            <!-- <a class="action-item add_to_cart float-right" data-id="{{$product->id}}">
                                                                <i class="fas fa-plus"></i>
                                                            </a> -->
                                                        @endif
                                                    </div>
                                                    <div class="card-image text-center p-0">
                                                        <figure class="figure">
                                                            @if(!empty($product->is_cover))
                                                                <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/'.$product->is_cover))}}" class="img-center pro_max_width60">
                                                            @else
                                                                <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/default.jpg'))}}" class="img-center pro_max_width60">
                                                            @endif
                                                        </figure>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center mt-4">
															<span class="h6 mb-0">
																{{\App\Utility::priceFormat($product->price)}}
															</span>
                                                                @if($product->quantity == 0)
                                                                    <span class="badge badge-pill badge-soft-danger ml-auto">
																{{__('Out of stock')}}
															</span>
                                                                @else
                                                                    <span class="badge badge-pill badge-soft-success ml-auto">
																{{__('In stock')}}
															</span>
                                                                @endif
                                                        </div>
                                                        <span class="static-rating static-rating-sm d-block">
															@if($store->enable_rating == 'on')
                                                                @for($i =1;$i<=5;$i++)
                                                                    @php
                                                                        $icon = 'fa-star';
                                                                        $color = '';
                                                                        $newVal1 = ($i-0.5);
                                                                        if($product->product_rating() < $i && $product->product_rating() >= $newVal1)
                                                                        {
                                                                            $icon = 'fa-star-half-alt';
                                                                        }
                                                                        if($product->product_rating() >= $newVal1)
                                                                        {
                                                                            $color = 'text-warning';
                                                                        }
                                                                    @endphp
                                                                    <i class="fas {{$icon .' '. $color}}"></i>
                                                                @endfor
                                                            @endif
														</span>
                                                    </div>
                                                </div>
												 <!-- Modal -->
                                                <div class="modal modal-up fade" id="{!! str_replace(' ', '_', $key)!!}_dialog_confirm_map_{{$product->id}}" style="overflow:scroll;" tabindex="-1" role="dialog" aria-labelledby="dialog_confirm_mapLabel" aria-hidden="true">													
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <div class="col-md-4">
                                                                    <h6> Choose Team Member(s) For This</h6>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <h6> Product Image</h6>
                                                                </div>                                                                    
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row" style="max-height:230px">
                                                                    <div class="col-sm-4 col-md-4 team_members" id="{!! str_replace(' ', '_', $key)!!}_dialog_confirm_map_{{$product->id}}_modal-body_{{$product->id}}" style="border-right: 2px solid #eff2f7;">
                                                                        <!-- HERE -->
                                                                    </div>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <div class="modal_product_image">
                                                                            @if(!empty($product->is_cover))
                                                                                <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/'.$product->is_cover))}}" class="img-center pro_max_width60">
                                                                            @else
                                                                                <img alt="Image placeholder" src="{{asset(Storage::url('uploads/is_cover_image/default.jpg'))}}" class="img-center pro_max_width60">
                                                                            @endif
                                                                        </div>
                                                                        <div class="product_description">
                                                                            <p>{{$product->description}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"> 
                                                                <div class="col-md-6">
                                                                    <label style="font-size:12px">Add Team Members</label>
                                                                    <input type="text" style="display:inline;width: 70%;height:auto;padding: 0.3rem;" name="new_member" class="form-control" id="">
                                                                    <button class="btn btn-xs btn-success ml-2 add_member"><i class="fas fa-plus"></i></button>
                                                                </div>
                                                                <div class="col-md-6" style="align-self: flex-end;">
                                                                    <div class="btn-group pull-right">
                                                                        <button data-modal="_{{$product->id}}" data-id="{{$product->id}}" data-main="{!! str_replace(' ', '_', $key)!!}_dialog_confirm_map_{{$product->id}}" class="add_to_cart btn-xs btn btn-success">Submit</button>
                                                                        <button data-dismiss="modal" class="btn-xs cancel-btn btn btn-danger">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div>
												<!-- /.modal -->
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endforeach
                </div>
                @if($store->enable_subscriber == 'on')
                    @if(!empty($store->sub_img))
                        <section class="slice slice-xl bg-cover bg-size--cover" style="background-image: url({{asset(Storage::url('uploads/store_logo/'.$store->sub_img))}}); background-position: center center;">
                            @else
                                <section class="slice slice-xl bg-cover bg-size--cover" style="background-image: url({{asset(Storage::url('uploads/store_logo/img-17.jpg'))}}); background-position: center center;">
                                    @endif
                                    <span class="mask bg-dark opacity-2"></span>
                                    <div class="container py-6">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6 col-xl-6 text-center">
                                                <div class="mb-5">
                                                    <h1 class="text-white">{{!empty($store->subscriber_title)?$store->subscriber_title:'Always on time'}}</h1>
                                                    <p class="lead text-white mt-2">{{!empty($store->sub_title)?$store->sub_title:'Subscription here'}}</p>
                                                </div>
                                                {{Form::open(array('route' => array('subscriptions.store_email', $store->id),'method' => 'POST'))}}
                                                <div class="form-group mb-0">
                                                    <div class="input-group input-group-lg input-group-merge rounded-pill bg-white">

                                                        {{Form::email('email',null,array('class'=>'form-control form-control-flush','aria-label'=>'Enter your email address','placeholder'=>__('Enter Your Email Address')))}}
                                                        <div class="input-group-append">
                                                            <button class="btn btn-white text-dark" type="submit">
                                                                <span class="far fa-paper-plane"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{Form::close()}}
                                            </div>
                                        </div>
                                    </div>
                                </section>
        @endif
</div>
<footer id="footer-main">
    <div class="footer footer-dark pt-4 pb-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright text-sm font-weight-bold text-center text-md-left pt-2">
                        {{$store->footer_note}}
                    </div>
                </div>
                <div class="col-md-6">
                    <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                        @if(!empty($store->email))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->email}}" target="_blank">
                                    <i class="fas fa-envelope"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->whatsapp))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->whatsapp}}" target=”_blank”>
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->facebook))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->facebook}}" target="_blank">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->instagram))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->instagram}}" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->twitter))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->twitter}}" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if(!empty($store->youtube))
                            <li class="nav-item">
                                <a class="nav-link" href="{{$store->youtube}}" target="_blank">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset('assets/js/site.core.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js')}}"></script>
<script src="{{ asset('assets/libs/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('assets/libs/swiper/dist/js/swiper.min.js')}}"></script>
<script src="{{asset('assets/js/site.js')}}"></script>
<script src="{{asset('assets/js/demo.js')}}"></script>

@php
    $store_settings = \App\Store::where('slug',$store->slug)->first();
@endphp
{!! $store_settings->storejs !!}
<script async src="https://www.googletagmanager.com/gtag/js?id={{$store_settings->google_analytic}}"></script>

<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '{{ $store_settings->google_analytic }}');
</script>

@if(Session::has('success'))
    <script>
        show_toastr('{{__('Success')}}', '{!! session('success') !!}', 'success');
    </script>
    {{ Session::forget('success') }}
@endif
@if(Session::has('error'))
    <script>
        show_toastr('{{__('Error')}}', '{!! session('error') !!}', 'error');
    </script>
    {{ Session::forget('error') }}
@endif


<script>
    let member_names = [];
    
    function fetchMembers(product_id = null){
         $.ajax({
            type: "GET",
            url: '{{route('user.getMembers', [$store->slug])}}',
            data: {
                "_token": "{{ csrf_token() }}",
                "product_id": product_id
            },
            success: function (response) {
                console.log(response);
                if (response.status == "Success" && response.team_members && response.team_members) {
                    let team_elem = $(".team_members");
                    team_elem.children().remove();
                    for (const [key, value] of Object.entries(response.team_members)) {
                        team_elem.append(`<div>
                            <input type="checkbox" name="team_member[]" value="${key}" id="" ${value}> <span class="member_name"> ${key.toUpperCase()} </span>
                        </div>`);    
                    }
                }
                // if (response.status == "Success" && response.team_members && response.team_members.length) {
                //     member_names = response.team_members
                //     response.team_members.forEach(member => {
                //         console.log(member);
                //     });
                // }

            },
            error: function (result) {
                show_toastr('Error', 'Something went wrong!', 'error');
                console.log('error',result);
            }
        });
    }
    $("button.add_member").on("click",function(){
        const memberElem = $(this).siblings("input[name='new_member']")[0]
        const member_name = memberElem.value
        if(member_name && member_name.trim().length && !member_names.includes(member_name.trim().toLowerCase())){
            let member = member_name.trim().toLowerCase()
             $.ajax({
                type: "POST",
                url: '{{route('user.storeMember', [$store->slug])}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "team_member": member
                },
                success: function (response) {
                    if (response.status == "Success") {
                         $(".team_members").append(`<div>
                            <input type="checkbox" name="team_member[]" value="${response.team_member}" id=""> <span class="member_name"> ${member} </span>
                        </div>`);
                        memberElem.value = ""
                        member_names.push(member);
                        show_toastr('Success', response.success, 'success');
                    }

                },
                error: function (result) {
                    show_toastr('Error', 'Something went wrong!', 'error');
                    console.log('error',result);
                }
            });
           
        }
    })
    //getting click event to show modal
    $('.submit-button').click(function (e) {
		e.preventDefault();
		let id = $(this).data("id")
		let modalId = $(this).data("modalid")
        fetchMembers(id);
		$(`#${modalId}`).modal();  
      //appending modal background inside the bigform-content
		$('.modal-backdrop').appendTo('.bigform-content');
      //removing body classes to able click events
		$('body').removeClass();
    });
    $(".add_to_cart").click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var modalId = $(this).data('main');
        var selected_member_names = $(`#${modalId}_modal-body${$(this).data("modal")}`).find("input:checked").map(function(){
            return $(this).val();
        }).get();
        if(!selected_member_names || selected_member_names.length < 1){
            show_toastr('Error', "You need to select atleast one member", 'error');
            return;
        }
        $.ajax({
            type: "POST",
            url: '{{route('user.addToCart', ['__product_id',$store->slug])}}'.replace('__product_id', id),
            data: {
                "_token": "{{ csrf_token() }}",
                "team_members": selected_member_names
            },
            success: function (response) {
                if (response.status == "Success") {
                    show_toastr('Success', response.success, 'success');
                    $("#shoping_count").attr("value", response.item_count);
                    $(`#${modalId}`).modal('toggle'); // closing the modal
                } else {
                    show_toastr('Error', response.error, 'error');
                }
            },
            error: function (result) {
                console.log('error');
            }
        });
    });
    $(".productTab").click(function (e) {
        e.preventDefault();
        $('.productTab').removeClass('active')

    });

    $("#pro_scroll").click(function () {
        $('html, body').animate({
            scrollTop: $("#pro_items").offset().top
        }, 1000);
    });
</script>
</body>

</html>

