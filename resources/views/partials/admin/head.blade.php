@php
    $logo=asset(Storage::url('uploads/logo/'));
    $favicon=\App\Utility::getValByName('favicon');

@endphp
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="StoreGo - Business Sales CRM">
    <meta name="author" content="Rajodiya Infotech">


    <title>@yield('page-title') - {{(\App\Utility::getValByName('title_text')) ? \App\Utility::getValByName('title_text') : config('app.name', 'StoreGo')}}</title>
    @if(!empty($favicon))
        <link rel="icon" href="{{asset(Storage::url('uploads/logo/'.$favicon))}}" type="image/png">
    @else
        <link rel="icon" href="{{asset(Storage::url('uploads/logo/favicon.png'))}}" type="image/png">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('assets/libs/@fortawesome/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/fullcalendar/dist/fullcalendar.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/libs/animate.css/animate.min.css')}}" id="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/site.css')}}" id="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css')}}">

        <link rel="stylesheet" href="{{ asset('assets/css/site-'.Auth::user()->mode.'.css') }}" id="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" id="stylesheet')}}">
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.1.min.js')}}"></script>

    @stack('css-page')
</head>
