<?php

namespace App\Http\Controllers;

use App\Location;
use App\Order;
use App\Plan;
use App\Product;
use App\Product_images;
use App\ProductCategorie;
use App\ProductVariantOption;
use App\Ratting;
use App\Shipping;
use App\Store;
use App\User;
use App\UserDetail;
use App\UserStore;
use App\Utility;
use Egulias\EmailValidator\EmailLexer;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use MongoDB\Driver\Session;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PhpParser\Node\Stmt\DeclareDeclare;
use Ramsey\Uuid\Type\Decimal;
use Shetabit\Visitor\Models\Visit;
use function Decimal\Decimal;
use function Psy\sh;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $lang = session()->get('lang');
        \App::setLocale(isset($lang) ? $lang : 'en');
    }

    public function index()
    {
        //        return view('store.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objStore = Store::create(
            [
                'created_by' => \Auth::user()->id,
                'name' => $request['store_name'],
                'currency_code' => 'USD',
                'paypal_mode' => 'sandbox',
            ]
        );

        \Auth::user()->current_store = $objStore->id;
        \Auth::user()->save();
        UserStore::create(
            [
                'user_id' => \Auth::user()->id,
                'store_id' => $objStore->id,
                'permission' => 'Owner',
            ]
        );

        return redirect()->back()->with('Success', 'Successfully Created' . $request['store_name'] . ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Store $store
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Store $store
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Store $store
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Store $store
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }

    public function savestoresetting(Request $request, $id)
    {
        $validator = \Validator::make(
            $request->all(), [
                               'name' => 'required|max:120',
                               'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                           ]
        );
        if($request->enable_domain == 'on')
        {
            $validator = \Validator::make(
                $request->all(), [
                                   'domains' => 'required',
                               ]
            );
        }
        if($validator->fails())
        {
            $messages = $validator->getMessageBag();

            return redirect()->back()->with('error', $messages->first());
        }
        if(!empty($request->logo))
        {
            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension       = $request->file('logo')->getClientOriginalExtension();
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            $dir             = storage_path('uploads/store_logo/');
            if(!file_exists($dir))
            {
                mkdir($dir, 0777, true);
            }
            $path = $request->file('logo')->storeAs('uploads/store_logo/', $fileNameToStore);
        }
        if(!empty($request->header_img))
        {
            $filenameWithExt      = $request->file('header_img')->getClientOriginalName();
            $filename             = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension            = $request->file('header_img')->getClientOriginalExtension();
            $fileNameToheader_img = $id . '_header_img_' . time() . '.' . $extension;
            $dir                  = storage_path('uploads/store_logo/');
            if(!file_exists($dir))
            {
                mkdir($dir, 0777, true);
            }
            $path = $request->file('header_img')->storeAs('uploads/store_logo/', $fileNameToheader_img);
        }
        if(!empty($request->sub_img))
        {
            $filenameWithExt   = $request->file('sub_img')->getClientOriginalName();
            $filename          = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension         = $request->file('sub_img')->getClientOriginalExtension();
            $fileNameTosub_img = $id . '_sub_img_' . time() . '.' . $extension;
            $dir               = storage_path('uploads/store_logo/');
            if(!file_exists($dir))
            {
                mkdir($dir, 0777, true);
            }
            $path = $request->file('sub_img')->storeAs('uploads/store_logo/', $fileNameTosub_img);
        }
        if($request->enable_domain == 'on')
        {
            // Remove the http://, www., and slash(/) from the URL
            $input = $request->domains;
            // If URI is like, eg. www.way2tutorial.com/
            $input = trim($input, '/');
            // If not have http:// or https:// then prepend it
            if(!preg_match('#^http(s)?://#', $input))
            {
                $input = 'http://' . $input;
            }
            $urlParts = parse_url($input);
            // Remove www.
            $domain_name = preg_replace('/^www\./', '', $urlParts['host']);
            // Output way2tutorial.com
        }
        else
        {
            $domain_name = '';
        }

        $store                      = Store::find($id);
        $store['name']              = $request->name;
        $store['email']             = $request->email;
        $store['enable_domain']     = $request->enable_domain ?? 'off';
        $store['domains']           = $domain_name;
        $store['about']             = $request->about;
        $store['tagline']           = $request->tagline;
        $store['lang']              = $request->store_default_language;
        $store['storejs']           = $request->storejs;
        $store['whatsapp']          = $request->whatsapp;
        $store['facebook']          = $request->facebook;
        $store['instagram']         = $request->instagram;
        $store['twitter']           = $request->twitter;
        $store['youtube']           = $request->youtube;
        $store['google_analytic']   = $request->google_analytic;
        $store['footer_note']       = $request->footer_note;
        $store['enable_header_img'] = $request->enable_header_img ?? 'off';
        if(!empty($fileNameToheader_img))
        {
            $store['header_img'] = $fileNameToheader_img;
        }
        $store['header_title']      = $request->header_title;
        $store['header_desc']       = $request->header_desc;
        $store['button_text']       = $request->button_text;
        $store['enable_subscriber'] = $request->enable_subscriber ?? 'off';
        $store['enable_rating']     = $request->enable_rating ?? 'off';
        $store['enable_shipping']   = $request->enable_shipping ?? 'off';
        if(!empty($fileNameTosub_img))
        {
            $store['sub_img'] = $fileNameTosub_img;
        }
        $store['subscriber_title'] = $request->subscriber_title;
        $store['sub_title']        = $request->sub_title;
        $store['address']          = $request->address;
        $store['city']             = $request->city;
        $store['state']            = $request->state;
        $store['zipcode']          = $request->zipcode;
        $store['country']          = $request->country;
        if(!empty($fileNameToStore))
        {
            $store['logo'] = $fileNameToStore;
        }
        $store['created_by'] = \Auth::user()->creatorId();
        $store->update();

        return redirect()->back()->with('success', __('Store successfully Update.'));
    }

    public function storeSlug($slug='')
    {
        if(!Auth::check())
        {
            visitor()->visit($slug);
        }

        $store     = Store::where('slug', $slug)->first();
        $userstore = UserStore::where('store_id', $store->id)->first();
        $settings  = \DB::table('settings')->where('name', 'company_favicon')->where('created_by', $userstore->user_id)->first();

        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }
        session(['slug' => $slug]);
        $cart = session()->get($slug);

        $categories = ProductCategorie::where('store_id', $userstore->store_id)->get()->pluck('name', 'id');
        $categories->prepend('All', 0);
        $products = [];
        foreach($categories as $id => $category)
        {
            $product = Product::where('store_id', $store->id);
            if($id != 0)
            {
                $product->where('product_categorie', $id);
            }
            $product             = $product->get();
            $products[$category] = $product;
        }

        if(isset($cart) && !empty($cart['products']))
        {
            $total_item = count($cart['products']);
        }
        else
        {
            $total_item = 0;
        }

        return view('storefront.index', compact('products', 'settings', 'store', 'categories', 'total_item'));
    }

    public function getMembers(Request $request,$slug){
        $team_members = session()->get($slug."_members");
        $cart    = session()->get($slug);
        if($request->product_id && $cart){
            foreach($cart as $product){
                    foreach($product as $team_member){
                        foreach($team_member as $member){
                            // dd($member['product_id'] == $request->product_id);?
                            if($member['product_id'] == $request->product_id && array_key_exists($member['team_member'],$team_members)){
                                $team_members[$member['team_member']] = 'checked';
                            }
                            else if($member['product_id'] == $request->product_id && !array_key_exists($member['team_member'],$team_members)){
                                $team_members[$member['team_member']] = '';
                                
                            }
                        }
                    }
                }
        }
    
        return response()->json(
            [
                'code' => 200,
                'status' => 'Success',
                'team_members' => $team_members
            ]
        );
    }
    public function storeMember(Request $request,$slug){        
        $team_members = session()->get($slug."_members") ?? array();
        $team_members[$request->team_member]='';
        session()->put($slug."_members",$team_members);
        return response()->json(
            [
                'code' => 200,
                'status' => 'Success',
                'team_member' => $request->team_member,
                'team_members' => $team_members,
                'success' => $request->team_member . __(' added to list successfully!'),
            ]
        );
    }
    public function productView($slug, $id)
    {
        $product_ratings = Ratting::where('slug', $slug)->where('product_id', $id)->get();
        $store_setting   = Store::where('slug', $slug)->first();
        $cart            = session()->get($slug);

        $ratting    = Ratting::where('product_id', $id)->where('rating_view', 'on')->sum('ratting');
        $user_count = Ratting::where('product_id', $id)->where('rating_view', 'on')->count();
        if($user_count > 0)
        {
            $avg_rating = number_format($ratting / $user_count, 1);
        }
        else
        {
            $avg_rating = number_format($ratting / 1, 1);

        }


        $store = Store::where('slug', $slug)->first();
        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }
        $products = Product::where('id', $id)->first();

        $products_image = Product_images::where('product_id', $products->id)->get();


        if(isset($cart) && !empty($cart['products']))
        {
            $total_item = count($cart['products']);
        }
        else
        {
            $total_item = 0;
        }

        $variant_name          = json_decode($products->variants_json);
        $product_variant_names = $variant_name;

        return view('storefront.view', compact('products', 'store', 'user_count', 'avg_rating', 'products_image', 'total_item', 'product_ratings', 'store_setting', 'product_variant_names'));
    }

    public function StoreCart($slug)
    {
        $store = Store::where('slug', $slug)->first();
        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }
        $cart = session()->get($slug);

        if(!empty($cart))
        {
            $products = $cart;
        }
        else
        {
            $products = '';
        }

        return view('storefront.cart', compact('products', 'store'));
    }

    public function userAddress($slug)
    {
        $store     = Store::where('slug', $slug)->first();
        $locations = Location::where('store_id', $store->id)->get()->pluck('name', 'id');
        $locations->prepend('Select Location');
        $shippings = Shipping::where('store_id', $store->id)->get();


        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }
        $cart = session()->get($slug);
        if(!empty($cart))
        {
            $products = $cart['products'];
        }
        else
        {
            return redirect()->back()->with('error', __('Please add to product into cart'));
        }
        if(!empty($cart['customer']))
        {
            $cust_details = $cart['customer'];
        }
        else
        {
            $cust_details = '';
        }
        $tax_name  = [];
        $tax_price = [];
        $i         = 0;
        if(!empty($products))
        {
            foreach($products as $team_products){
                foreach($team_products as $product)
                {
                    if($product['variant_id'] != 0)
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            if(!in_array($taxs['tax_name'], $tax_name))
                            {
                                $tax_name[]  = $taxs['tax_name'];
                                $price       = $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;
                                $tax_price[] = $price;
                            }
                            else
                            {
                                $price                                                 = $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;
                                $tax_price[array_search($taxs['tax_name'], $tax_name)] += $price;
                            }
                        }
                    }
                    else
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            if(!in_array($taxs['tax_name'], $tax_name))
                            {
                                $tax_name[]  = $taxs['tax_name'];
                                $price       = $product['price'] * $product['quantity'] * $taxs['tax'] / 100;
                                $tax_price[] = $price;
                            }
                            else
                            {
                                $price                                                 = $product['price'] * $product['quantity'] * $taxs['tax'] / 100;
                                $tax_price[array_search($taxs['tax_name'], $tax_name)] += $price;
                            }
                        }
                    }
                    $i++;
                }
            }
            $total_item     = $i;
            $taxArr['tax']  = $tax_name;
            $taxArr['rate'] = $tax_price;

            return view('storefront.shipping', compact('products', 'store', 'taxArr', 'total_item', 'cust_details', 'locations', 'shippings'));
        }
        else
        {

            return redirect()->back()->with('error', __('Please add to product into cart.'));
        }
    }

    public function UserLocation($slug, $location_id)
    {
        $store     = Store::where('slug', $slug)->first();
        $shippings = Shipping::where('store_id', $store->id)->whereRaw('FIND_IN_SET("' . $location_id . '", location_id)')->get()->toArray();

        return response()->json(
            [
                'code' => 200,
                'status' => 'Success',
                'shipping' => $shippings,
            ]
        );
    }

    public function UserShipping(Request $request, $slug, $shipping_id)
    {
        $store = Store::where('slug', $slug)->first();

        $shippings       = Shipping::where('store_id', $store->id)->where('id', $shipping_id)->first();
        $shipping_price  = Utility::priceFormat($shippings->price);
        $pro_total_price = str_replace(',', '', str_replace('$', '', $request->pro_total_price));
        $total_price     = Utility::priceFormat(number_format($shippings->price) + $pro_total_price);

        return response()->json(
            [
                'code' => 200,
                'status' => 'Success',
                'price' => $shipping_price,
                'total_price' => $total_price,
            ]
        );
    }

    public function userPayment($slug)
    {
        $store = Store::where('slug', $slug)->first();
        $order = Order::where('user_id', $store->id)->orderBy('id', 'desc')->first();

        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }
        $cart = session()->get($slug);
        if(!empty($cart))
        {
            $products = $cart['products'];
        }
        else
        {
            return redirect()->back()->with('error', __('Please add to product into cart'));
        }

        if(!empty($cart['customer']))
        {
            $cust_details = $cart['customer'];
        }
        else
        {
            return redirect()->back()->with('error', __('Please add your information'));
        }
        if($store->enable_shipping == 'on')
        {
            if(!empty($cart['shipping']))
            {
                $shipping         = $cart['shipping'];
                $shipping_details = Shipping::where('store_id', $store->id)->where('id', $shipping['shipping_id'])->first();
                $shipping_price   = $shipping_details->price;
            }
            else
            {
                return redirect()->back()->with('error', __('Please Select Shipping Location'));
            }
        }
        else
        {
            $shipping_price = 0;
        }

        $store     = Store::where('slug', $slug)->first();
        $tax_name  = [];
        $tax_price = [];
        $i         = 0;
        if(!empty($products))
        {
            if(!empty($cust_details))
            {
                foreach($products as $team_products)
                {
                    foreach($team_products as $product)
                    {
                        if($product['variant_id'] != 0)
                        {
                            foreach($product['tax'] as $key => $taxs)
                            {

                                if(!in_array($taxs['tax_name'], $tax_name))
                                {
                                    $tax_name[]  = $taxs['tax_name'];
                                    $price       = $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;
                                    $tax_price[] = $price;
                                }
                                else
                                {
                                    $price                                                 = $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;
                                    $tax_price[array_search($taxs['tax_name'], $tax_name)] += $price;
                                }
                            }
                        }
                        else
                        {

                            foreach($product['tax'] as $key => $taxs)
                            {
                                if(!in_array($taxs['tax_name'], $tax_name))
                                {
                                    $tax_name[]  = $taxs['tax_name'];
                                    $price       = $product['price'] * $product['quantity'] * $taxs['tax'] / 100;
                                    $tax_price[] = $price;
                                }
                                else
                                {
                                    $price                                                 = $product['price'] * $product['quantity'] * $taxs['tax'] / 100;
                                    $tax_price[array_search($taxs['tax_name'], $tax_name)] += $price;
                                }
                            }
                        }
                        $i++;
                    }
                }
                $encode_product = json_encode($products);
                $total_item     = $i;
                $taxArr['tax']  = $tax_name;
                $taxArr['rate'] = $tax_price;

                // For Url
                $pro_qty  = [];
                $pro_name = [];
                if(!empty($order))
                {
                    $order_id = '%23' . str_pad($order->id + 1, 4, "100", STR_PAD_LEFT);
                }
                else
                {
                    $order_id = '%23' . str_pad(0 + 1, 4, "100", STR_PAD_LEFT);
                }

                foreach($products as $team_items){
                    foreach($team_items as $item)
                    {
                        $pro_qty[] = $item['quantity'] . ' x ' . $item['product_name'];
                    }
                }

                $url = 'https://api.whatsapp.com/send?phone=' . env('WHATSAPP_NUMBER') . '&text=Hi%2C%0AWelcome+to+%2A' . $store->name . '%2A%2C%0AYour+order+is+confirmed+%26+your+order+no.+is+' . $order_id . '%0AYour+order+detail+is%3A%0A%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%0A+' . join("+%2C%0A", $pro_qty) . '+%0A+%0A%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%7E%0ATo+collect+the+order+you+need+to+show+the+receipt+at+the+counter.%0A%0AThanks+' . $store->name . '%0A%0A';

                return view('storefront.payment', compact('products', 'order', 'cust_details', 'store', 'taxArr', 'total_item', 'encode_product', 'url', 'shipping_price'));
            }
            else
            {
                return redirect()->back()->with('error', __('Please fill your details.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('Please add to product into cart.'));
        }
    }

    public function addToCart(Request $request, $product_id, $slug, $variant_id = 0)
    {
        if($request->ajax())
        {
            $store   = Store::where('slug', $slug)->get();
            $variant = ProductVariantOption::find($variant_id);
            if(empty($store))
            {
                return redirect()->back()->with('error', __('Store not available'));
            }

            $product = Product::find($product_id);
            $cart    = session()->get($slug);
            // dd($cart);

            $quantity = $product->quantity;
            if($variant_id > 0)
            {
                $quantity = $variant->quantity;
            }

            if(!empty($product->is_cover))
            {
                $pro_img = $product->is_cover;
            }
            else
            {
                $pro_img = 'default.jpg';
            }

            $productquantity = $product->quantity;
            $i               = 0;

            //            if(!$product && $quantity == 0)
            if($quantity == 0)
            {
                return response()->json(
                    [
                        'code' => 404,
                        'status' => 'Error',
                        'error' => __('This product is out of stock!'),
                    ]
                );
            }
            if($quantity < count($request->team_members))
            {
                return response()->json(
                    [
                        'code' => 404,
                        'status' => 'Error',
                        'error' => __('Stock is Limited!'),
                    ]
                );
            }

            $productname      = $product->name;
            $productprice     = $product->price != 0 ? $product->price : 0;
            $originalquantity = (int)$productquantity;

            //product count tax
            $taxes      = Utility::tax($product->product_tax);
            $itemTaxes  = [];
            $producttax = 0;

            if(!empty($taxes))
            {
                foreach($taxes as $tax)
                {
                    if(!empty($tax))
                    {
                        $producttax          = Utility::taxRate($tax->rate, $product->price, 1);
                        $itemTax['tax_name'] = $tax->name;
                        $itemTax['tax']      = $tax->rate;
                        $itemTaxes[]         = $itemTax;
                    }
                }
            }

            $subtotal = Utility::priceFormat($productprice + $producttax);

            if($variant_id > 0)
            {
                $variant_itemTaxes       = [];
                $variant_name            = $variant->name;
                $variant_price           = $variant->price;
                $originalvariantquantity = (int)$variant->quantity;
                //variant count tax
                $variant_taxes      = Utility::tax($product->product_tax);
                $variant_producttax = 0;

                if(!empty($variant_taxes))
                {
                    foreach($variant_taxes as $variant_tax)
                    {
                        if(!empty($variant_tax))
                        {
                            $variant_producttax  = Utility::taxRate($variant_tax->rate, $variant_price, 1);
                            $itemTax['tax_name'] = $variant_tax->name;
                            $itemTax['tax']      = $variant_tax->rate;
                            $variant_itemTaxes[] = $itemTax;
                        }
                    }
                }
                $variant_subtotal = Utility::priceFormat($variant_price * $variant->quantity);
            }

            $time = time();
            // if cart is empty then this the first product
            if(!$cart || !$cart['products'])
            {
                $cart['products'][$time] = array();
                foreach($request->team_members as $team_member){
                    if($variant_id > 0)
                    {
                        array_push($cart['products'][$time],[
                            "product_id" => $product->id,
                            "product_name" => $productname,
                            "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                            "quantity" => 1,
                            "price" => $productprice,
                            "id" => $product_id,
                            "team_member" => $team_member,
                            "tax" => $variant_itemTaxes,
                            "subtotal" => $subtotal,
                            "originalquantity" => $originalquantity,
                            "variant_name" => $variant_name,
                            "variant_price" => $variant_price,
                            "variant_qty" => $variant->quantity,
                            "variant_subtotal" => $variant_subtotal,
                            "originalvariantquantity" => $originalvariantquantity,
                            'variant_id' => $variant_id,
                        ]);
                        $originalquantity -=1;
                    }
                    else if($variant_id <= 0)
                    {
                        array_push($cart['products'][$time],[
                            "product_id" => $product->id,
                            "product_name" => $productname,
                            "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                            "quantity" => 1,
                            "price" => $productprice,
                            "id" => $product_id,
                            "team_member" => $team_member,
                            "tax" => $itemTaxes,
                            "subtotal" => $subtotal,
                            "originalquantity" => $originalquantity,
                            'variant_id' => 0,
                        ]);
                        $originalquantity -=1;
                    }
                }

                session()->put($slug, $cart);

                return response()->json(
                    [
                        'code' => 200,
                        'status' => 'Success',
                        'success' => $productname . __(' added to cart successfully!'),
                        'cart' => $cart['products'],
                        'item_count' => count($cart['products']),
                    ]
                );
            }

            // if cart not empty then check if this product exist then increment quantity
            $new_members = $request->team_members; //checking if a new member added or not
            if($variant_id > 0)
            {
                $key = false;
                $team_memb_key = '';
                foreach($cart['products'] as $tk => $team_members)
                {
                    foreach($team_members as $k => $value){
                        array_search($value['team_member'],$new_members);
                        if($variant_id == $value['variant_id'])
                        {
                            $team_memb_key = $tk;
                            $key = $k;
                        }
                    }
                }

                if($key !== false && isset($cart['products'][$key]['variant_id']) && $cart['products'][$key]['variant_id'] != 0)
                {
                    if(isset($cart['products'][$key]))
                    {
                        foreach(array_unique($new_members) as $team_member){
                            if($team_member != "0"){
                                array_push($cart['products'][$team_memb_key],[
                                    "product_id" => $product->id,
                                    "product_name" => $productname,
                                    "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                                    "quantity" => 1,
                                    "price" => $productprice,
                                    "id" => $product_id,
                                    "team_member" => $team_member,
                                    "tax" => $variant_itemTaxes,
                                    "subtotal" => $subtotal,
                                    "originalquantity" => $originalquantity,
                                    "variant_name" => $variant_name,
                                    "variant_price" => $variant_price,
                                    "variant_qty" => $variant->quantity,
                                    "variant_subtotal" => $variant_subtotal,
                                    "originalvariantquantity" => $originalvariantquantity,
                                    'variant_id' => $variant_id,
                                ]);
                                if($originalvariantquantity < $cart['products'][$key]['quantity'])
                                {
                                    return response()->json(
                                        [
                                            'code' => 404,
                                            'status' => 'Error',
                                            'error' => __('This product is out of stock!'),
                                        ]
                                    );
                                    break;
                                }
                                $originalvariantquantity-=1;
                            }
                        }
                        // $cart['products'][$key]['quantity']         = $cart['products'][$key]['quantity'] + 1;
                        // $cart['products'][$key]['variant_subtotal'] = $cart['products'][$key]['variant_price'] * $cart['products'][$key]['quantity'];

                        session()->put($slug, $cart);

                        return response()->json(
                            [
                                'code' => 200,
                                'status' => 'Success',
                                'success' => $productname . __(' added to cart successfully!'),
                                'cart' => $cart['products'],
                                'item_count' => count($cart['products']),
                            ]
                        );
                    }
                }
            }
            else if($variant_id <= 0)
            {
                $key = false;
                $team_memb_key = '';
                foreach($cart['products'] as $tk => $team_members)
                {
                    foreach($team_members as $k => $value){
                        if($product_id == $value['product_id']){
                            // dump(in_array($value['team_member'],$new_members));
                            if(!in_array($value['team_member'],$new_members))
                            {
                                array_push($new_members,$value['team_member']);
                                    $team_memb_key = $tk;
                                    $key = $k;
                            }else{
                                unset($cart['products'][$tk][$k]);
                                if(!count($cart['products'][$tk])){
                                    unset($cart['products'][$tk]);
                                }
                            }
                        }
                    }
                }
                
                if($key !== false)
                {
                    
                    if(isset($cart['products'][$team_memb_key][$key]))
                    {
                        foreach(array_unique($new_members) as $team_member){
                            if($team_member != "0"){
                                 $originalquantity = ($product->quantity-1);
                                array_push($cart['products'][$team_memb_key],[
                                    "product_id" => $product->id,
                                    "product_name" => $productname,
                                    "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                                    "quantity" => 1,
                                    "price" => $productprice,
                                    "id" => $product_id,
                                    "team_member" => $team_member,
                                    "tax" => $itemTaxes,
                                    "subtotal" => $subtotal,
                                    "originalquantity" => $originalquantity,
                                    'variant_id' => 0,
                                ]);
                                // $cart['products'][$team_memb_key][$key]['quantity'] = $cart['products'][$team_memb_key][$key]['quantity'] + 1;
                                // $cart['products'][$team_memb_key][$key]['subtotal'] = $cart['products'][$team_memb_key][$key]['price'] * $cart['products'][$team_memb_key][$key]['quantity'];
                                if($originalquantity < $cart['products'][$team_memb_key][$key]['quantity'])
                                {
                                    return response()->json(
                                        [
                                            'code' => 404,
                                            'status' => 'Error',
                                            'error' => __('This product is out of stock!'),
                                        ]
                                    );
                                }
                                $originalquantity-=1;
                            }
                        }

                        session()->put($slug, $cart);

                        return response()->json(
                            [
                                'code' => 200,
                                'status' => 'Success',
                                'success' => $productname . __(' added to cart successfully!'),
                                'cart' => $cart['products'],
                                'item_count' => count($cart['products']),
                            ]
                        );
                    }
                }
            }
            $cart['products'][$time] = array();
            foreach($request->team_members as $team_member){
                // if item not exist in cart then add to cart with quantity = 1
                if($variant_id > 0)
                {
                    array_push($cart['products'][$time],[
                        "product_id" => $product->id,
                        "product_name" => $productname,
                        "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                        "quantity" => 1,
                        "price" => $productprice,
                        "id" => $product_id,
                        "team_member" => $team_member,
                        "tax" => $variant_itemTaxes,
                        "subtotal" => $subtotal,
                        "originalquantity" => $originalquantity,
                        "variant_name" => $variant->name,
                        "variant_price" => $variant->price,
                        "variant_qty" => $variant->quantity,
                        "variant_subtotal" => $variant_subtotal,
                        "originalvariantquantity" => $originalvariantquantity,
                        'variant_id' => $variant_id,
                    ]);
                    $originalquantity -=1;
                }
                else if($variant_id <= 0)
                {
                    array_push($cart['products'][$time],[
                        "product_id" => $product->id,
                        "product_name" => $productname,
                        "image" => Storage::url('uploads/is_cover_image/' . $pro_img),
                        "quantity" => 1,
                        "team_member" => $team_member,
                        "price" => $productprice,
                        "id" => $product_id,
                        "tax" => $itemTaxes,
                        "subtotal" => $subtotal,
                        "originalquantity" => $originalquantity,
                        'variant_id' => 0,
                    ]);
                    $originalquantity -=1;

                }
            }

            session()->put($slug, $cart);

            return response()->json(
                [
                    'code' => 200,
                    'status' => 'Success',
                    'success' => $productname . __(' added to cart successfully!'),
                    'cart' => $cart['products'],
                    'item_count' => count($cart['products']),
                ]
            );
        }
    }

    public function productqty(Request $request, $product_id, $slug, $key = 0)
    {
        $cart = session()->get($slug);
        if($cart['products'][$key]['variant_id'] > 0 && $cart['products'][$key]['originalvariantquantity'] < $request->product_qty)
        {
            return response()->json(
                [
                    'code' => 404,
                    'status' => 'Error',
                    'error' => __('You can only purchese max') . ' ' . $cart['products'][$key]['originalvariantquantity'] . ' ' . __('product!'),
                ]
            );
        }
        else if($cart['products'][$key]['originalquantity'] < $request->product_qty)
        {
            return response()->json(
                [
                    'code' => 404,
                    'status' => 'Error',
                    'error' => __('You can only purchese max') . ' ' . $cart['products'][$key]['originalquantity'] . ' ' . __('product!'),
                ]
            );
        }

        if(isset($cart['products'][$key]))
        {
            $cart['products'][$key]['quantity'] = $request->product_qty;
            $cart['products'][$key]['id']       = $product_id;

            $subtotal = $cart['products'][$key]["price"] * $cart['products'][$key]["quantity"];

            $protax = $cart['products'][$key]["tax"];
            if($protax != 0)
            {
                $taxs = 0;
                foreach($protax as $tax)
                {
                    $taxs += ($subtotal * $tax['tax']) / 100;
                }
            }
            else
            {
                $taxs = 0;
                $taxs += ($subtotal * 0) / 100;

            }
            $cart['products'][$key]["subtotal"] = $subtotal + $taxs;

            session()->put($slug, $cart);

            return response()->json(
                [
                    'code' => 200,
                    'status' => 'Success',
                    'success' => $cart['products'][$key]["product_name"] . __(' added to cart successfully!'),
                    'product' => $cart['products'],
                    'carttotal' => $cart['products'],
                ]
            );

        }
    }

    public function delete_cart_item($slug, $id, $team_member, $variant_id = 0)
    {
        $cart = session()->get($slug);
        foreach($cart['products'] as $k => $team_members)
        {
            foreach($team_members as $key => $product)
            {
                if(($variant_id > 0 && $product['variant_id'] == $variant_id) && $team_member == $product['team_member'])
                {
                    if(count($cart['products'][$k]) == 1)
                        unset($cart['products'][$k]);
                    else
                        unset($cart['products'][$k][$key]);
                }
                else if($cart['products'][$k][$key]['product_id'] == $id && $variant_id == 0 && $team_member == $product['team_member'])
                {
                    if(count($cart['products'][$k]) == 1)
                        unset($cart['products'][$k]);
                    else
                        unset($cart['products'][$k][$key]);
                }

            }
        }

        $cart['products'] = array_values($cart['products']);

        session()->put($slug, $cart);

        return redirect()->back()->with('success', __('Item successfully Deleted.'));
    }

    public function customer(Request $request, $slug)
    {
        $store = Store::where('slug', $slug)->first();

        if(empty($store))
        {
            return redirect()->back()->with('error', __('Store not available'));
        }

        $cart = session()->get($slug);

        $products = $cart['products'];

        $validator = \Validator::make(
            $request->all(), [
                               'name' => 'required|max:120',
                               'last_name' => 'required|max:120',
                               'billing_postalcode' => 'required|numeric',
                               'shipping_postalcode' => 'required|numeric',
                           ]
        );
        if($validator->fails())
        {
            $messages = $validator->getMessageBag();

            return redirect()->back()->with('error', $messages->first());
        }
        if($store->enable_shipping == "on")
        {
            if($request->location_id == 0 && empty($request->location_id))
            {
                return redirect()->back()->with('error', __('Please Select Location'));
            }
            if(empty($request->shipping_id))
            {
                return redirect()->back()->with('error', __('Please Select Shipping Method'));
            }
        }
        else
        {
            $request->location_id = 0;
            $request->shipping_id = 0;
        }

        if($request->location_id != 0 && !empty($request->location_id) && !empty($request->shipping_id))
        {
            $cart['shipping'] = [
                'location_id' => $request->location_id,
                'shipping_id' => $request->shipping_id,
            ];
        }

        $userdetail                        = new UserDetail();
        $userdetail['store_id']            = $store->id;
        $userdetail['name']                = $request->name;
        $userdetail['last_name']           = $request->last_name;
        $userdetail['email']               = $request->email;
        $userdetail['phone']               = $request->phone;
        $userdetail['billing_address']     = $request->billing_address;
        $userdetail['billing_country']     = $request->billing_country;
        $userdetail['billing_city']        = $request->billing_city;
        $userdetail['billing_postalcode']  = $request->billing_postalcode;
        $userdetail['shipping_address']    = $request->shipping_address;
        $userdetail['shipping_country']    = $request->shipping_country;
        $userdetail['shipping_city']       = $request->shipping_city;
        $userdetail['shipping_postalcode'] = $request->shipping_postalcode;
        $userdetail['location_id']         = $request->location_id;
        $userdetail['shipping_id']         = $request->shipping_id;
        $userdetail->save();
        $userdetail->id;


        $cart['customer'] = [
            "id" => $userdetail->id,
            "name" => $request->name,
            "last_name" => $request->last_name,
            "phone" => $request->phone,
            "email" => $request->email,
            "billing_address" => $request->billing_address,
            "billing_country" => $request->billing_country,
            "billing_city" => $request->billing_city,
            "billing_postalcode" => $request->billing_postalcode,
            "shipping_address" => $request->shipping_address,
            "shipping_country" => $request->shipping_country,
            "shipping_city" => $request->shipping_city,
            "shipping_postalcode" => $request->shipping_postalcode,
            "location_id" => $request->location_id,
            "shipping_id" => $request->shipping_id,
        ];

        session()->put($slug, $cart);

        return redirect()->route('store-payment.payment', $slug);

    }

    public function complete($slug, $order_id)
    {
        $order = Order::where('id', Crypt::decrypt($order_id))->first();
        $store = Store::where('slug', $slug)->first();

        return view('storefront.complete', compact('slug', 'store', 'order_id', 'order'));
    }

    public function userorder($slug, $order_id)
    {
        $id    = Crypt::decrypt($order_id);
        $store = Store::where('slug', $slug)->first();
        $order = Order::where('id', $id)->first();
        if(!empty($order->shipping_data))
        {
            $shipping_data = json_decode($order->shipping_data);
            $location_data = Location::where('id', $shipping_data->location_id)->first();
        }
        else
        {
            $shipping_data = '';
            $location_data = '';
        }

        $user_details   = UserDetail::where('id', $order->user_address_id)->first();
        $order_products = json_decode($order->product);

        $sub_total = 0;
        if(!empty($order_products))
        {
            $grand_total = 0;
            $total_taxs  = 0;
            foreach($order_products as $team_products){
                foreach($team_products as $product)
                {
                    if($product->variant_id != 0)
                    {
                        if(!empty($product->tax))
                        {
                            foreach($product->tax as $tax)
                            {
                                $sub_tax    = ($product->variant_price * $product->quantity * $tax->tax) / 100;
                                $total_taxs += $sub_tax;
                            }
                        }

                        $totalprice  = $product->variant_price * $product->quantity + $total_taxs;
                        $subtotal    = $product->variant_price * $product->quantity;
                        $sub_total   += $subtotal;
                        $grand_total += $totalprice;
                    }
                    else
                    {
                        if(!empty($product->tax))
                        {
                            foreach($product->tax as $tax)
                            {
                                $sub_tax    = ($product->price * $product->quantity * $tax->tax) / 100;
                                $total_taxs += $sub_tax;
                            }
                        }

                        $totalprice  = $product->price * $product->quantity + $total_taxs;
                        $subtotal    = $product->price * $product->quantity;
                        $sub_total   += $subtotal;
                        $grand_total += $totalprice;

                    }
                }
            }
        }

        return view('storefront.userorder', compact('slug', 'store', 'order', 'grand_total', 'order_products', 'sub_total', 'total_taxs', 'user_details', 'shipping_data', 'location_data'));
    }

    public function whatsapp(Request $request, $slug)
    {
        $validator = \Validator::make(
            $request->all(), [
                               'wts_number' => 'required',
                           ]
        );
        if($validator->fails())
        {
            // $messages = $validator->getMessageBag();

            return response()->json(
                [
                    'status' => 'error',
                    'success' => __('The Phone number field is required.'),
                ]
            );
        }
        $store        = Store::where('slug', $slug)->first();
        $products     = $request['product'];
        $order_id     = $request['order_id'];
        $cart         = session()->get($slug);
        $cust_details = $cart['customer'];

        $product_name = [];
        $product_id   = [];
        $tax_name     = [];
        $totalprice   = 0;
        foreach($products as $team_products){
            foreach($team_products as $key => $product)
            {
                if($product['variant_id'] == 0)
                {
                    $new_qty                = $product['originalquantity'] - $product['quantity'];
                    $product_edit           = Product::find($product['product_id']);
                    $product_edit->quantity = $new_qty;
                    $product_edit->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
                elseif($product['variant_id'] != 0)
                {
                    $new_qty                   = $product['originalvariantquantity'] - $product['quantity'];
                    $product_variant           = ProductVariantOption::find($product['variant_id']);
                    $product_variant->quantity = $new_qty;
                    $product_variant->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['variant_price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
            }
        }
        if(isset($cart['shipping']) && isset($cart['shipping']['shipping_id']) && !empty($cart['shipping']))
        {
            $shipping = Shipping::find($cart['shipping']['shipping_id']);
            if(!empty($shipping))
            {
                $totalprice     = $totalprice + $shipping->price;
                $shipping_name  = $shipping->name;
                $shipping_price = $shipping->price;
                $shipping_data  = json_encode(
                    [
                        'shipping_name' => $shipping_name,
                        'shipping_price' => $shipping_price,
                        'location_id' => $cart['shipping']['location_id'],
                    ]
                );
            }
            else
            {
                $shipping_data = '';
            }
        }
        if($product)
        {
            $order                  = new Order();
            $order->order_id        = $order_id;
            $order->name            = $cust_details['name'];
            $order->card_number     = '';
            $order->card_exp_month  = '';
            $order->card_exp_year   = '';
            $order->status          = 'pending';
            $order->phone           = $request->wts_number;
            $order->user_address_id = $cust_details['id'];
            $order->shipping_data   = $shipping_data;
            $order->product_id      = implode(',', $product_id);
            $order->price           = $totalprice;
            $order->product         = json_encode($products);
            $order->price_currency  = $store->currency_code;
            $order->txn_id          = '';
            $order->payment_type    = __('Whatsapp');
            $order->payment_status  = 'approved';
            $order->receipt         = '';
            $order->user_id         = $store['id'];
            $order->save();

            session()->forget($slug);

            return response()->json(
                [
                    'status' => 'success',
                    'success' => __('Your Order Successfully Added'),
                    'order_id' => Crypt::encrypt($order->id),
                ]
            );
        }
        else
        {
            return redirect()->back()->with('error', __('failed'));
        }
    }

    public function cod(Request $request, $slug)
    {
        $store        = Store::where('slug', $slug)->first();
        $products     = $request['product'];
        $order_id     = $request['order_id'];
        $cart         = session()->get($slug);
        $cust_details = $cart['customer'];

        $product_name = [];
        $product_id   = [];
        $tax_name     = [];
        $totalprice   = 0;
        foreach($products as $team_products){
            foreach($team_products as $key => $product)
            {
                if($product['variant_id'] == 0)
                {
                    $new_qty                = $product['originalquantity'] - $product['quantity'];
                    $product_edit           = Product::find($product['product_id']);
                    $product_edit->quantity = $new_qty;
                    $product_edit->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
                elseif($product['variant_id'] != 0)
                {
                    $new_qty                   = $product['originalvariantquantity'] - $product['quantity'];
                    $product_variant           = ProductVariantOption::find($product['variant_id']);
                    $product_variant->quantity = $new_qty;
                    $product_variant->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['variant_price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
            }
        }
        if(isset($cart['shipping']) && isset($cart['shipping']['shipping_id']) && !empty($cart['shipping']))
        {
            $shipping = Shipping::find($cart['shipping']['shipping_id']);
            if(!empty($shipping))
            {
                $totalprice     = $totalprice + $shipping->price;
                $shipping_name  = $shipping->name;
                $shipping_price = $shipping->price;
                $shipping_data  = json_encode(
                    [
                        'shipping_name' => $shipping_name,
                        'shipping_price' => $shipping_price,
                        'location_id' => $cart['shipping']['location_id'],
                    ]
                );
            }
            else
            {
                $shipping_data = '';
            }
        }

        if($product)
        {
            $order                  = new Order();
            $order->order_id        = $order_id;
            $order->name            = $cust_details['name'];
            $order->card_number     = '';
            $order->card_exp_month  = '';
            $order->card_exp_year   = '';
            $order->status          = 'pending';
            $order->user_address_id = $cust_details['id'];
            $order->shipping_data   = $shipping_data;
            $order->product_id      = implode(',', $product_id);
            $order->price           = $totalprice;
            $order->product         = json_encode($products);
            $order->price_currency  = $store->currency_code;
            $order->txn_id          = '';
            $order->payment_type    = __('COD');
            $order->payment_status  = 'approved';
            $order->receipt         = '';
            $order->user_id         = $store['id'];
            $order->save();

            session()->forget($slug);

            return response()->json(
                [
                    'status' => 'success',
                    'success' => __('Your Order Successfully Added'),
                    'order_id' => Crypt::encrypt($order->id),
                ]
            );
        }
        else
        {
            return redirect()->back()->with('error', __('failed'));
        }
    }

    public function bank_transfer(Request $request, $slug)
    {
        $store        = Store::where('slug', $slug)->first();
        $products     = $request['product'];
        $order_id     = $request['order_id'];
        $cart         = session()->get($slug);
        $cust_details = $cart['customer'];

        $product_name = [];
        $product_id   = [];
        $tax_name     = [];
        $totalprice   = 0;
        foreach($products as $team_products){
            foreach($team_products as $key => $product)
            {
                if($product['variant_id'] == 0)
                {
                    $new_qty                = $product['originalquantity'] - $product['quantity'];
                    $product_edit           = Product::find($product['product_id']);
                    $product_edit->quantity = $new_qty;
                    $product_edit->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
                elseif($product['variant_id'] != 0)
                {
                    $new_qty                   = $product['originalvariantquantity'] - $product['quantity'];
                    $product_variant           = ProductVariantOption::find($product['variant_id']);
                    $product_variant->quantity = $new_qty;
                    $product_variant->save();

                    $tax_price = 0;
                    if(!empty($product['tax']))
                    {
                        foreach($product['tax'] as $key => $taxs)
                        {
                            $tax_price += $product['variant_price'] * $product['quantity'] * $taxs['tax'] / 100;

                        }
                    }
                    $totalprice     += $product['variant_price'] * $product['quantity'] + $tax_price;
                    $product_name[] = $product['product_name'];
                    $product_id[]   = $product['id'];
                }
            }
        }
        if(isset($cart['shipping']) && isset($cart['shipping']['shipping_id']) && !empty($cart['shipping']))
        {
            $shipping = Shipping::find($cart['shipping']['shipping_id']);
            if(!empty($shipping))
            {
                $totalprice     = $totalprice + $shipping->price;
                $shipping_name  = $shipping->name;
                $shipping_price = $shipping->price;
                $shipping_data  = json_encode(
                    [
                        'shipping_name' => $shipping_name,
                        'shipping_price' => $shipping_price,
                        'location_id' => $cart['shipping']['location_id'],
                    ]
                );
            }
            else
            {
                $shipping_data = '';
            }
        }
        if($product)
        {
            $order                  = new Order();
            $order->order_id        = $order_id;
            $order->name            = $cust_details['name'];
            $order->card_number     = '';
            $order->card_exp_month  = '';
            $order->card_exp_year   = '';
            $order->status          = 'pending';
            $order->user_address_id = $cust_details['id'];
            $order->shipping_data   = $shipping_data;
            $order->product_id      = implode(',', $product_id);
            $order->price           = $totalprice;
            $order->product         = json_encode($products);
            $order->price_currency  = $store->currency_code;
            $order->txn_id          = '';
            $order->payment_type    = __('Bank Transfer');
            $order->payment_status  = 'approved';
            $order->receipt         = '';
            $order->user_id         = $store['id'];
            $order->save();

            session()->forget($slug);

            return response()->json(
                [
                    'status' => 'success',
                    'success' => __('Your Order Successfully Added'),
                    'order_id' => Crypt::encrypt($order->id),
                ]
            );
        }
        else
        {
            return redirect()->back()->with('error', __('failed'));
        }
    }

    public function changeCurrantStore($storeID)
    {
        $objStore = Store::find($storeID);
        if($objStore->is_active)
        {
            $objUser                = Auth::user();
            $objUser->current_store = $storeID;
            $objUser->update();

            return redirect()->route('dashboard')->with('success', __('Store Change Successfully!'));
        }
        else
        {
            return redirect()->back()->with('error', __('Store is locked'));
        }
    }

    public function ownerstoredestroy($id)
    {
        $user        = Auth::user();
        $store       = Store::find($id);
        $user_stores = UserStore::where('user_id', $user->id)->count();

        if($user_stores > 1)
        {
            UserStore::where('store_id', $id)->delete();
            $store->delete();

            $userstore = UserStore::where('user_id', $user->id)->first();

            $user->current_store = $userstore->id;
            $user->save();

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->back()->with('error', __('You have only one store'));
        }


    }
}
